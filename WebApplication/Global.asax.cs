using High.System.Frame;
using LibraryCore;
using LibraryCore.Core;
using LibraryCore.Data;
using RazorView.Core;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            try
            {

                AreaRegistration.RegisterAllAreas();
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                GlobalConfiguration.Configure(WebApiConfig.Register);
                RouteConfig.RegisterRoutes(RouteTable.Routes);

                //自定义视图注册
                ViewEngines.Engines.Clear();
                ViewEngines.Engines.Add(new CustomViewEngines());

            }
            catch (Exception ex)
            {
                LogNet.WriteLog("WEB API zhu", ex);
            }

            // 检查系统的安装锁
            try
            {
                if (ConfigHelper.ReadOtherConfigFile("InstallLock").Equals("true"))
                {
                    ConfigHelper.WriteOtherConfigFile("DefultAction", "Install|InstallApplication");
                    HttpRuntime.UnloadAppDomain();
                }
                else
                {
                    ConfigHelper.WriteOtherConfigFile("DefultAction", "Administrator|Login");
                }
            }
            catch (Exception ex)
            {
                LogNet.WriteLog("Global：检查系统安装锁", ex);
            }

        }
    }
}
