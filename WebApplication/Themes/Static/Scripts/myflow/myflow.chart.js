(function ($) {
    var myflow = $.myflow;

    $.extend(true, myflow.config.rect, {
        attr: {
            r: 8,
            fill: '#F6F7FF',
            stroke: '#03689A',
            "stroke-width": 2
        }
    });

    $.extend(true, myflow.config.props.props, {
        name: { name: 'name', label: '名称', value: '新建流程', editor: function () { return new myflow.editors.inputEditor(); } },
        desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
    });


    $.extend(true, myflow.config.tools.states, {
        start: {
            showType: 'image',
            type: 'start',
            name: { text: '<<start>>' },
            text: { text: '开始' },
            img: { src: '/Themes/Static/Images/myflow//48/start_event_empty.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                key: { name: 'name', label: '标识', value: '开始', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        end: {
            showType: 'image', type: 'end',
            name: { text: '<<end>>' },
            text: { text: '结束' },
            img: { src: '/Themes/Static/Images/myflow//48/end_event_terminate.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                key: { name: 'name', label: '标识', value: '结束', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: ' ', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        'end-cancel': {
            showType: 'image', type: 'end-cancel',
            name: { text: '<<end-cancel>>' },
            text: { text: '取消' },
            img: { src: '/Themes/Static/Images/myflow//48/end_event_cancel.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                key: { name: 'name', label: '标识', value: '取消', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: ' ', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        'end-error': {
            showType: 'image', type: 'end-error',
            name: { text: '<<end-error>>' },
            text: { text: '错误' },
            img: { src: '/Themes/Static/Images/myflow//48/end_event_error.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                key: { name: 'name', label: '标识', value: '错误', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: ' ', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        state: {
            showType: 'text', type: 'state',
            name: { text: '<<state>>' },
            text: { text: '状态' },
            img: { src: '/Themes/Static/Images/myflow//48/task_empty.png', width: 48, height: 48 },
            props: {
                key: { name: 'name', label: '标识', value: '状态', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: ' ', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        fork: {
            showType: 'image', type: 'fork',
            name: { text: '<<fork>>' },
            text: { text: '分支' },
            img: { src: '/Themes/Static/Images/myflow//48/gateway_parallel.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                key: { name: 'name', label: '标识', value: '分支', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: ' ', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        join: {
            showType: 'image', type: 'join',
            name: { text: '<<join>>' },
            text: { text: '合并' },
            img: { src: '/Themes/Static/Images/myflow//48/gateway_parallel.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                key: { name: 'name', label: '标识', value: '合并', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: ' ', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        },
        task: {
            showType: 'text', type: 'task',
            name: { text: '<<task>>' },
            text: { text: '任务' },
            img: { src: '/Themes/Static/Images/myflow//48/task_empty.png', width: 48, height: 48 },
            props: {
                key: { name: 'name', label: '标识', value: '任务', editor: function () { return new myflow.editors.inputReadonlyEditor(); } },
                name: { name: 'name', label: '名称', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
                desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.mutilTextEditor(); } }
            }
        }
    });

})(jQuery);