﻿
var Queue;
function QueueInit() {
    this.handlers = {};
    this._events = {};
    this.host = "127.0.0.1";
    this.port = "15674";
    this.uname = "guest";
    this.upwd = "guest";
    this.queue = "";
    this.State = "";
    this.msg = "";
    this.object = "";
    this.client = null;
    this.ws = null;
}


QueueInit.prototype.render = function () {
    //this.init(); 
}

QueueInit.prototype = {
    Init: function (queue, f) {
        Queue = this;
        if (f) {
            $.get("/MQ/TestQueue?fanout=".format(queue));
        }
        $.post("/MQ/GetMqConfig", function (data) {
            if (data.State !== "Success") {
                console.log("ERROR:{0}".format(data.Message));
            }

            Queue.host = data.Object["host"];
            Queue.port = data.Object["port"];
            Queue.uname = data.Object["uName"];
            Queue.upwd = data.Object["uPwd"];
            Queue.queue = queue;
            Queue.Connect();
        }, "json");
        return this;
    },
    Connect: function () {
        if (Queue.client === null || !Queue.client.connected) {
            Queue.ws = new WebSocket('ws://{0}:{1}/ws'.format(Queue.host, Queue.port));
            Queue.client = Stomp.over(Queue.ws);
            Queue.client.heartbeat.outgoing = 0;
            Queue.client.heartbeat.incoming = 0;
            Queue.client.connect(Queue.uname, Queue.upwd, Queue.Meaasge, Queue.Error, '/');
        }
    },
    Meaasge: function (x) {
        Queue.client.subscribe(Queue.queue, function (data) {
            Queue.State = "Success";
            var msg = data.body;
            Queue.object = JSON.parse(msg);
            Queue.fireEvent("Message", msg);
        });
    },
    Error: function (e) {
        Queue.State = "Error";
        Queue.fireEvent("Message", "错误提醒：{0}".format(e));

        window.setTimeout(() => {
            Queue.Connect();
        }, 5000)
    }

}

QueueInit.prototype.addEventListener = function (type, listener, capture) {

    // 判断一下传入的参数是否符合规格
    if (typeof type !== "string" || typeof listener !== "function") return;

    // 缓存符合条件的事件列表
    var list = this._events[type];

    // 判断是否已经有该类型事件，若没有则添加一个新数组
    if (typeof list === "undefined") list = (this._events[type] = []);

    /* 判断插入函数的位置 */
    if (!!capture) list.push(listener);
    else list.insert(0, listener);

    return this;
};

QueueInit.prototype.removeEventListener = function (type, listener, capture) {
    // 判断一下传入的参数是否符合规格
    if (typeof type !== "string" || typeof listener !== "function") return this;

    // 缓存符合条件的事件列表
    var list = this._events[type];

    // 若没有绑定过此类事件则不需要做处理
    if (typeof list === "undefined") return this;

    for (var i = 0, len = list.length; i < len; i++) {
        // 通过循环判断来确定事件列表中存在要移除的事件侦听函数
        if (list[i] == listener) {
            // 找到后将此侦听函数从事件列表中移除
            list.remove(i);
            break;
        }
    }
    return this;
};

QueueInit.prototype.fireEvent = function (type, e) {
    // 若存在DOM0用法的函数，则触发
    this["on" + type.toLowerCase()] && this["on" + type.toLowerCase()].call(this, e);

    // 缓存符合条件的事件列表
    var list = this._events[type];

    // 若事件列表中没有内容则不需要做处理
    if (!list || list.length <= 0) return this;

    // 阻止事件冒泡开关
    var isStop = false;

    // 模拟事件对象
    window.event = { stopPropagation: function () { isStop = true; } };
    e.stopPropagation = window.event.stopPropagation;

    for (var i = 0, len = list.length; i < len; i++) {
        // 通过循环触发符条件的事件列表中存在的所有事件侦听函数
        // 若函数内返回false或事件内调用了event.stopPropagation函数则阻止接下来的所有调用
        if (list[i].call(this, e) === false || isStop) break;
    }
    return this;
};

Array.prototype.insert = function (index, value) {

    if (index > this.length) index = this.length;
    if (index < -this.length) index = 0;
    if (index < 0) index = this.length + index;
    for (var i = this.length; i > index; i--) {
        this[i] = this[i - 1];
    }
    this[index] = value;
    return this;
};

Array.prototype.remove = function (index) {
    if (isNaN(index) || index > this.length) return;
    this.splice(index, 1);
};


String.prototype.format = function () {
    if (arguments.length == 0) return this;
    var param = arguments[0];
    var s = this;
    if (typeof (param) == 'object') {
        for (var key in param)
            s = s.replace(new RegExp("\\{" + key + "\\}", "g"), param[key]);
        return s;
    } else {
        for (var i = 0; i < arguments.length; i++)
            s = s.replace(new RegExp("\\{" + i + "\\}", "g"), arguments[i]);
        return s;
    }
}