﻿using LibraryAdministrator.ActionFilter;
using LibraryCore;
using LibraryCore.Core;
using LibraryCore.Data;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using TengYunCore;
using static System.Net.Mime.MediaTypeNames;

namespace RazorView.Controllers
{
    public class SysInfoController : Controller
    {
        /// <summary>
        /// 获取系统引用信息
        /// </summary>
        /// <returns></returns>
        public string PlugsInfo()
        {
            var result = new Result();
            try
            {
                var array = AppDomain.CurrentDomain.GetAssemblies().ToList().OrderBy(x => x.GetName().Name);
                var list = new List<PlugsInfo>();
                foreach (Assembly item in array.Where(x => x.GetName().Name.ToLower().Contains("library") || x.GetName().Name == "WebApplication"))
                {

                    try
                    {
                        var attributes = item.GetCustomAttribute(typeof(AssemblyDescriptionAttribute));
                        var desc = ((AssemblyDescriptionAttribute)attributes)?.Description;

                        if (string.Join(".", item.GetName().Version) == "0.0.0.0" || string.IsNullOrWhiteSpace(desc))
                        {
                            continue;
                        }

                        var version = string.Join(".", item.GetName().Version);
                        version = version.Split('.').Last().Length > 3
                            ? version.Substring(0, version.Length - 2)
                            : version;
                        list.Add(new PlugsInfo { PlugName = item.GetName().Name, PlugVersion = version, PlugDesc = desc });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                result.Object = list;
                result.State = ResultState.Success;
                result.Message = "获取信息成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }

            return result.ToString();
        }

        /// <summary>
        /// 系统重启接口
        /// </summary>
        /// <returns></returns>
        public string RestartSystem()
        {

            var result = new Result();
            try
            {
                HttpRuntime.UnloadAppDomain();
                result.State = ResultState.Success;
                result.Message = "系统重启通知完成，稍后短时间内将无法访问请注意。";
            }
            catch (System.Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"系统重启接口 RestartSystem", ex);
            }
            return result.ToString();
        }
    }
}