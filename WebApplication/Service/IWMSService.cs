﻿using LibraryModel.OtherModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBasicApi.Interface
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码和配置文件中的接口名“IService1”。
    [ServiceContract]
    public interface IWmsService
    {
        //收货
        [OperationContract]
        string DeleteReceive(string id);

        [OperationContract]
        string GetReceive();

        [OperationContract]
        string GetReceiveOfid(string id);

        [OperationContract]
        string PostReceive(string data);

        [OperationContract]
        string PutReceive(string data);

        //出库
        [OperationContract]
        string DeleteStockOut(string id);

        [OperationContract]
        string GetStockOut();

        [OperationContract]
        string GetStockOutOfid(string id);

        [OperationContract]
        string PostStockOut(string data);

        [OperationContract]
        string PutStockOut(string data);

        //质检
        [OperationContract]
        string PostQualityTest(string data);
    }
}
