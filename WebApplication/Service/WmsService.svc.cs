﻿using LibraryBasicApi.Interface;
using LibraryBasicApi.WMS.BLL;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;

namespace LibraryBasicApi.WebService
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码、svc 和配置文件中的类名“Service1”。
    // 注意: 为了启动 WCF 测试客户端以测试此服务，请在解决方案资源管理器中选择 Service1.svc 或 Service1.svc.cs，然后开始调试。
    public class WmsService : IWmsService
    {
        //public string DeleteMateria(string id)
        //{
        //    MateriaApi mw = new MateriaApi();
        //    return JsonConvert.SerializeObject(mw.Delete(id));
        //}

        public string DeleteReceive(string id)
        {
            ReceiveApi rw = new ReceiveApi();
            return JsonConvert.SerializeObject(rw.Delete(id));
        }

        public string DeleteStockOut(string id)
        {
            StockOutApi sow = new StockOutApi();
            return JsonConvert.SerializeObject(sow.Delete(id));
        }

        //public string DeleteWarehouse(string id)
        //{
        //    WarehouseApi ww = new WarehouseApi();
        //    return JsonConvert.SerializeObject(ww.Delete(id));
        //}

        //public string DeleteWarehouseArea(string id)
        //{
        //    WarehouseAreaApi waw = new WarehouseAreaApi();
        //    return JsonConvert.SerializeObject(waw.Delete(id));
        //}

        //public string DeleteWarehouseLocation(string id)
        //{
        //    WarehouseLocationApi wlw = new WarehouseLocationApi();
        //    return JsonConvert.SerializeObject(wlw.Delete(id));
        //}

        //public string GetMateria()
        //{
        //    MateriaApi mw = new MateriaApi();
        //    return JsonConvert.SerializeObject(mw.Get());
        //}

        //public string GetMateriaOfid(string id)
        //{
        //    MateriaApi mw = new MateriaApi();
        //    return JsonConvert.SerializeObject(mw.Get(id));
        //}

        public string GetReceive()
        {
            ReceiveApi rw = new ReceiveApi();
            return JsonConvert.SerializeObject(rw.Get());
        }

        public string GetReceiveOfid(string id)
        {
            ReceiveApi rw = new ReceiveApi();
            return JsonConvert.SerializeObject(rw.Get(id));
        }

        public string GetStockOut()
        {
            StockOutApi sow = new StockOutApi();
            return JsonConvert.SerializeObject(sow.Get());
        }

        public string GetStockOutOfid(string id)
        {
            StockOutApi sow = new StockOutApi();
            return JsonConvert.SerializeObject(sow.Get(id));
        }

        //public string GetWarehouse()
        //{
        //    WarehouseApi ww = new WarehouseApi();
        //    return JsonConvert.SerializeObject(ww.Get());
        //}

        //public string GetWarehouseOfid(string id)
        //{
        //    WarehouseApi ww = new WarehouseApi();
        //    return JsonConvert.SerializeObject(ww.Get(id));
        //}

        //public string GetWarehouseArea()
        //{
        //    WarehouseAreaApi waw = new WarehouseAreaApi();
        //    return JsonConvert.SerializeObject(waw.Get());
        //}

        //public string GetWarehouseAreaOfid(string id)
        //{
        //    WarehouseAreaApi waw = new WarehouseAreaApi();
        //    return JsonConvert.SerializeObject(waw.Get(id));
        //}

        //public string GetWarehouseLocation()
        //{
        //    WarehouseLocationApi wlw = new WarehouseLocationApi();
        //    return JsonConvert.SerializeObject(wlw.Get());
        //}

        //public string GetWarehouseLocationOfid(string id)
        //{
        //    WarehouseLocationApi wlw = new WarehouseLocationApi();
        //    return JsonConvert.SerializeObject(wlw.Get(id));
        //}

        //public string PostMateria(object data)
        //{
        //    MateriaApi mw = new MateriaApi();
        //    return JsonConvert.SerializeObject(mw.Post(data));
        //}

        public string PostQualityTest(string data)
        {
            QualityTestingApi qt = new QualityTestingApi();
            return JsonConvert.SerializeObject(qt.Post(data));
        }

        public string PostReceive(string data)
        {
            ReceiveApi rw = new ReceiveApi();
            return JsonConvert.SerializeObject(rw.Post(data));
        }

        public string PostStockOut(string data)
        {
            StockOutApi sow = new StockOutApi();
            return JsonConvert.SerializeObject(sow.Post(data));
        }

        //public string PostWarehouse(object data)
        //{
        //    WarehouseApi ww = new WarehouseApi();
        //    return JsonConvert.SerializeObject(ww.Post(data));
        //}

        //public string PostWarehouseArea(object data)
        //{
        //    WarehouseAreaApi waw = new WarehouseAreaApi();
        //    return JsonConvert.SerializeObject(waw.Post(data));
        //}

        //public string PostWarehouseLocation(object data)
        //{
        //    WarehouseLocationApi wlw = new WarehouseLocationApi();
        //    return JsonConvert.SerializeObject(wlw.Post(data));
        //}

        //public string PutMateria(object data)
        //{
        //    MateriaApi mw = new MateriaApi();
        //    return JsonConvert.SerializeObject(mw.Put(data));
        //}

        public string PutReceive(string data)
        {
            ReceiveApi rw = new ReceiveApi();
            return JsonConvert.SerializeObject(rw.Put(data));
        }

        public string PutStockOut(string data)
        {
            StockOutApi sow = new StockOutApi();
            return JsonConvert.SerializeObject(sow.Put(data));
        }

        //public string PutWarehouse(object data)
        //{
        //    WarehouseApi ww = new WarehouseApi();
        //    return JsonConvert.SerializeObject(ww.Put(data));
        //}

        //public string PutWarehouseArea(object data)
        //{
        //    WarehouseAreaApi waw = new WarehouseAreaApi();
        //    return JsonConvert.SerializeObject(waw.Put(data));
        //}

        //public string PutWarehouseLocation(object data)
        //{
        //    WarehouseLocationApi wlw = new WarehouseLocationApi();
        //    return JsonConvert.SerializeObject(wlw.Put(data));
        //}
    }
}
