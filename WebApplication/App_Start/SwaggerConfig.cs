using System.Web.Http;
using WebActivatorEx;
using WebApplication;
using Swashbuckle.Application;
using System.Linq;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace WebApplication
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        //
                        c.SingleApiVersion("V1", "Web-App-API");
                        c.BasicAuth("Casso Team");
                        c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                        c.IncludeXmlComments(string.Format("{0}/bin/plugWms.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IncludeXmlComments(string.Format("{0}/bin/plugreport.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IncludeXmlComments(string.Format("{0}/bin/plugmobile.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IncludeXmlComments(string.Format("{0}/bin/plugMes.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IncludeXmlComments(string.Format("{0}/bin/plugEamd.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                        c.IncludeXmlComments(string.Format("{0}/bin/plugAdministrator.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                    })
                .EnableSwaggerUi(c =>
                    {

                    });
        }
    }
}
