﻿using LibraryCore.Data;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var action = ConfigHelper.ReadOtherConfigFile("DefultAction").Split('|');

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = action[0], action = action[1], id = UrlParameter.Optional }
            );
        }
    }
}
