﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class MoblieApiController : Controller
    {





        /// <summary>
        /// 品质分析不/合格率
        /// </summary>
        /// <returns></returns>
        public string QaData(string date)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {

                    var data1 = new Dictionary<string, object>();
                    var dateLocal = DateTime.Parse(date);
                    var dateLoca2 = dateLocal.AddMonths(+1).AddSeconds(-1);
                    var ks = dateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                    var js = dateLoca2.ToString("yyyy-MM-dd HH:mm:ss");

                    return new DataBaseHelper().OnlineSql(
                         $"select case when((select sum(sl) from jgx where kgrq>='{ks}'and kgrq<='{js}')) is null then 0 else (select sum(sl) from jgx where kgrq>='{ks}'and kgrq<='{js}')end as hg ,count(djbh) as bhg ,(select sum(fzbgs+fgssl)*100/sum(zbgs+gssl) from jgx where wgrq>='{ks}'and wgrq<='{js}' and zbgs+gssl>0) scxl,(select  COUNT(distinct jtbm) from jgx  where kgrq>='{ks}'and kgrq<='{js}' )scjt from vzl where substring(djbh,1,2)='bg' and rq>='{ks}'and rq<='{js}' and isnull(yy,'')<>''", true, 1).ToString();
                }
            }

            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);

        }


        /// <summary>
        /// 品质分析不合格原因及其统计
        /// </summary>
        /// <returns></returns>
        public string QaTopData(string date)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data1 = new Dictionary<string, object>();

                    var dateLocal = DateTime.Parse(date);
                    var dateLoca2 = dateLocal.AddMonths(+1).AddSeconds(-1);

                    var ks = dateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                    var js = dateLoca2.ToString("yyyy-MM-dd HH:mm:ss");

                    result = new DataBaseHelper().OnlineSql(
                        $"select yy name, count(djbh) value,(select sum(zcsl + xsl) from vzl where substring(djbh, 1, 2) = 'bg' and rq>= '{ks}'and rq<= '{js}') sumNum from vzl where substring(djbh, 1, 2) = 'bg' and rq>= '{ks}'and rq<= '{js}' and isnull(yy,'')<> '' group by yy order by COUNT(djbh) desc", true, 1);

                    //将数组数据分类为   name    和    value两大类
                    var obj = JsonConvert.DeserializeObject<List<KeyValue>>(JsonConvert.SerializeObject(result.Object));
                    data1.Add("name", obj.ConvertAll(x => x.Name));
                    data1.Add("value", obj.ConvertAll(x => x.Value));
                    result.Object = data1;
                }


            }

            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);


        }



        /// <summary>
        /// 查询发货及时率
        /// </summary>
        /// <returns></returns>
        public string TimelyDelivery(string date)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data1 = new Dictionary<string, object>();

                    var dateLocal = DateTime.Parse(date);
                    var dateWeek1 = dateLocal.AddDays(+7).AddSeconds(-1);
                    var dateWeek2 = dateWeek1.AddDays(+7);
                    var dateWeek3 = dateWeek2.AddDays(+7);
                    var dateLoca2 = dateLocal.AddMonths(+1).AddSeconds(-1);


                    var ks = dateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                    var week1 = dateWeek1.ToString("yyyy-MM-dd HH:mm:ss");
                    var week2 = dateWeek2.ToString("yyyy-MM-dd HH:mm:ss");
                    var week3 = dateWeek3.ToString("yyyy-MM-dd HH:mm:ss");
                    var js = dateLoca2.ToString("yyyy-MM-dd HH:mm:ss");

                    result = new DataBaseHelper().OnlineSql(
                        $@"select '1' name ,SUM(zshs) * 100 /count(djbh) value,
                        (select top 1 bl from bm where lx='zb' and mc='发货及时率' and bzbm=datepart(yy,getdate())) ParentID
                        from vdd   where isnull(zzbz,'')<>'1' and hdrq is not null and hdrq between '{ks}' and '{week1}'
                        union
                        select '2' name ,SUM(zshs) * 100 /count(djbh) value,
                        (select top 1 bl from bm where lx='zb' and mc='发货及时率' and bzbm=datepart(yy,getdate())) ParentID
                        from vdd   where isnull(zzbz,'')<>'1' and hdrq is not null and hdrq between '{week1}' and '{week2}'
                        union
                        select '3' name ,SUM(zshs) * 100 /count(djbh) value,
                        (select top 1 bl from bm where lx='zb' and mc='发货及时率' and bzbm=datepart(yy,getdate())) ParentID
                        from vdd   where isnull(zzbz,'')<>'1' and hdrq is not null and hdrq between '{week2}' and '{week3}'
                        union
                        select '4' name ,SUM(zshs) * 100 /count(djbh) value,
                        (select top 1 bl from bm where lx='zb' and mc='发货及时率' and bzbm=datepart(yy,getdate())) ParentID
                        from vdd   where isnull(zzbz,'')<>'1' and hdrq is not null and hdrq between '{week3}' and '{js}'
                        union
                        select '5' name ,count(zshs) value,ParentID=0
                        from vdd   where zshs=0 and isnull(zzbz,'')<>'1' and hdrq is not null and hdrq between '{ks}' and '{js}'
                        ", true, 1);



                    //将数组数据分类为   name    和    value两大类

                    var obj = JsonConvert.DeserializeObject<List<KeyValue>>(JsonConvert.SerializeObject(result.Object));
                    data1.Add("name", obj.ConvertAll(x => x.Name));
                    data1.Add("value", obj.ConvertAll(x => x.Value));
                    data1.Add("ParentID", obj.ConvertAll(x => x.ParentID));
                    result.Object = data1;
                }


            }

            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);




        }


        /// <summary>
        /// 发货不及时原因
        /// </summary>
        /// <returns></returns>
        public string NotTimelyDelivery(string date)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data = new Dictionary<string, object>();
                    var dateLocal = DateTime.Parse(date);
                    var dateLoca2 = dateLocal.AddMonths(+1).AddSeconds(-1);

                    var ks = dateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                    var js = dateLoca2.ToString("yyyy-MM-dd HH:mm:ss");

                    result = new DataBaseHelper().OnlineSql(
                        $@"select djbh,csmc,CONVERT(varchar(100), vdd.hdrq, 20)as jhrq,CONVERT(varchar(100), vdd.bgrq, 20)as hdrq,Convert(decimal(18,1),yqts sc from vdd   where zshs = 0 and isnull(zzbz, '') <> '1' and hdrq is not null and hdrq between '{ks}' and '{js}'", true, 1);

                    if (result.State != ResultState.Success)
                        return result.ToString();
                    data.Add("rwgx", result.Object);


                    result.Object = data;
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);

        }

        /// <summary>
        /// 查询采购及时率
        /// </summary>
        /// <returns></returns>
        public string PurchasingTimeliness(string date)//接收日期
        {
            var result = new Result();//声明
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    //返回值
                    var data1 = new Dictionary<string, object>();
                    var dateLocal = DateTime.Parse(date);
                    var dateWeek1 = dateLocal.AddDays(+7).AddSeconds(-1);
                    var dateWeek2 = dateWeek1.AddDays(+7);
                    var dateWeek3 = dateWeek2.AddDays(+7);
                    var dateLoca2 = dateLocal.AddMonths(+1).AddSeconds(-1);

                    var ks = dateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                    var week1 = dateWeek1.ToString("yyyy-MM-dd HH:mm:ss");
                    var week2 = dateWeek2.ToString("yyyy-MM-dd HH:mm:ss");
                    var week3 = dateWeek3.ToString("yyyy-MM-dd HH:mm:ss");
                    var js = dateLoca2.ToString("yyyy-MM-dd HH:mm:ss");
                    //处理月份（例：07转换成7）
                    var month = int.Parse(date.Substring(5, 2));
                    result = new DataBaseHelper().OnlineSql(
$@"
SELECT '1' name ,Convert(decimal(10,2),(sum(b{month}zssl)*100/sum(b{month}sl))) value
FROM vgdbly 
WHERE vgdbly.sl>0 and rq between '{ks}' and '{week1}'
UNION		
SELECT '2' name , Convert(decimal(10,2),(sum(b{month}zssl)*100/sum(b{month}sl))) value
FROM vgdbly 
WHERE vgdbly.sl>0 and rq between '{week1}' and '{week2}'
UNION	
SELECT '3' name , Convert(decimal(10,2),(sum(b{month}zssl)*100/sum(b{month}sl))) value
FROM vgdbly 
WHERE vgdbly.sl>0 and rq between '{week2}' and '{week3}'
UNION	
SELECT '4' name , Convert(decimal(10,2),(sum(b{month}zssl)*100/sum(b{month}sl))) value
FROM vgdbly 
WHERE vgdbly.sl>0 and rq between '{week3}' and '{js}'
                    ", true, 1);
                    //将数组数据分类为name、value、ParentID
                    var obj = JsonConvert.DeserializeObject<List<KeyValue>>(JsonConvert.SerializeObject(result.Object));

                    data1.Add("name", obj.ConvertAll(x => x.Name));
                    data1.Add("value", obj.ConvertAll(x => x.Value));
                    data1.Add("ParentID", obj.ConvertAll(x => x.ParentID));
                    result.Object = data1;
                }


            }

            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);




        }

        /// <summary>
        /// 采购不及时数量
        /// </summary>
        /// <returns></returns>
        public string PurchasingCount(string date)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    //返回值
                    var data = new Dictionary<string, object>();
                    //获取年份
                    var year = date.Substring(0, 4);
                    //获取月份
                    var months = date.Substring(5, 2);
                    //处理月份（例：07转换成7）
                    var month = int.Parse(date.Substring(5, 2));

                    result = new DataBaseHelper().OnlineSql(
                    $@"
                    SELECT case when (sum(b{month}sl)-sum(b{month}zssl) )is null then 0 else (sum(b{month}sl)-sum(b{month}zssl)) end as value, 
                    case when (sum(b{month}zssl))is null then 0 else (sum(b{month}zssl)) end as name, 
                    case when (sum(b{month}sl))is null then 0 else (sum(b{month}sl)) end as ParentID
                    FROM vgdbly 
                    WHERE vgdbly.sl>0 and substring(jhkjqj,1,4)='{year}' and qybm!=' ' and substring(jhkjqj,5,6)='{months}'
                     ", true, 1);

                    var obj = JsonConvert.DeserializeObject<List<KeyValue>>(JsonConvert.SerializeObject(result.Object));
                    data.Add("name", obj.ConvertAll(x => x.Name));//采购及时数量
                    data.Add("value", obj.ConvertAll(x => x.Value));//采购不及时数量
                    data.Add("ParentID", obj.ConvertAll(x => x.ParentID));//采购总数量

                    result.Object = data;
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);

        }

        /// <summary>
        /// 采购不及时明细
        /// </summary>
        /// <returns></returns>
        public string PurchasingCause(string date)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data = new Dictionary<string, object>();
                    var dateLocal = DateTime.Parse(date);
                    var dateLoca2 = dateLocal.AddMonths(+1).AddSeconds(-1);

                    var ks = dateLocal.ToString("yyyy-MM-dd HH:mm:ss");
                    var js = dateLoca2.ToString("yyyy-MM-dd HH:mm:ss");

                    result = new DataBaseHelper().OnlineSql(
                        $@"select djbh,csmc,CONVERT(varchar(100), vdd.jhrq, 20)as jhrq,CONVERT(varchar(100), vdd.bgrq, 20)as bgrq,Convert(decimal(18,1),DATEDIFF(hh,jhrq,bgrq)/24.0) sc from vdd   where zshs = 0 and isnull(zzbz, '') <> '1' and hdrq is not null and hdrq between '{ks}' and '{js}'", true, 1);

                    if (result.State != ResultState.Success)
                        return result.ToString();
                    data.Add("rwgx", result.Object);


                    result.Object = data;
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);

        }
    }
}

