﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryBasicApi.Report
{
    public class RotationApiController : ControllerExc
    {
        /// <summary>
        /// 获取轮播信息
        /// </summary>
        /// <returns></returns>
        public string GetRotationAndUrl(string field, string order, int page = 1, int limit = 10, string condition=null)
        {
            var result = new ResultDataTable();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    int count = 0;
                    List<RotationModel> lrm = new List<RotationModel>();
                    var spdList = db.Queryable<SysPageDisplay>();

                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(order))
                    {
                        spdList.OrderBy($" {field} {order}");
                    }

                    if (!string.IsNullOrEmpty(condition))
                    {
                        var obj = JsonConvert.DeserializeObject<ConditionStr>(condition);
                        var conditions = JsonConvert.DeserializeObject<List<Condition>>(obj.jsonStr);
                        foreach (var condition1 in conditions)
                        {

                            switch (condition1.ConditionOptionVal)
                            {
                                case "equal":
                                    spdList.Where($"{condition1.ConditionFieldVal}= '{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "unequal":
                                    spdList.Where($"{condition1.ConditionFieldVal} <> '{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "like":
                                    spdList.Where($"{condition1.ConditionFieldVal} like '%{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "unlike":
                                    spdList.Where($"{condition1.ConditionFieldVal} not like '%{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "start":
                                    spdList.Where($"{condition1.ConditionFieldVal}  like '{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "end":
                                    spdList.Where($"{condition1.ConditionFieldVal}  like '%{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "empty":
                                    spdList.Where($"{condition1.ConditionFieldVal} is null");
                                    break;
                                case "between":
                                    spdList.Where($"{condition1.ConditionFieldVal} BETWEEN {condition1.ConditionValueLeftVal} and {condition1.ConditionValueRightVal}");
                                    break;
                            }
                        }
                    }
                    foreach (var item in spdList.ToPageList(page, limit, ref count))
                    {
                        var spsList = db.Queryable<SysPageScrolling>().OrderBy(x=>x.ID).Where(x => x.PID == item.ID).ToList();
                        RotationModel rm = new RotationModel
                        {
                            ID=item.ID,
                            Name=item.Name,
                            ScrollingMode=item.ScrollingMode,
                            SwitchingTime=item.SwitchingTime,
                            State=item.State,
                            CreateTime=item.CreateTime,
                            CreateUser=item.CreateUser
                        };
                        string[] str = new string[10];
                        for (int i = 0; i < spsList.Count; i++)
                        {
                            str[i] = spsList[i].URL;
                        }
                        if (!string.IsNullOrEmpty(str[0]))
                            rm.URL1 = spsList[0].URL;
                        if (!string.IsNullOrEmpty(str[1]))
                            rm.URL2 = spsList[1].URL;
                        if (!string.IsNullOrEmpty(str[2]))
                            rm.URL3 = spsList[2].URL;
                        if (!string.IsNullOrEmpty(str[3]))
                            rm.URL4 = spsList[3].URL;
                        if (!string.IsNullOrEmpty(str[4]))
                            rm.URL5 = spsList[4].URL;
                        if (!string.IsNullOrEmpty(str[5]))
                            rm.URL6 = spsList[5].URL;
                        if (!string.IsNullOrEmpty(str[6]))
                            rm.URL7 = spsList[6].URL;
                        if (!string.IsNullOrEmpty(str[7]))
                            rm.URL8 = spsList[7].URL;
                        if (!string.IsNullOrEmpty(str[8]))
                            rm.URL9 = spsList[8].URL;
                        if (!string.IsNullOrEmpty(str[9]))
                            rm.URL10 = spsList[9].URL;
                        lrm.Add(rm);
                    }
                    result.data = lrm;
                    result.code = 0;
                    result.count = count;
                }
            }
            catch (Exception ex)
            {
                result.code = 1;
                result.msg = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 新增轮播主表和子表
        /// </summary>
        /// <param name="ID">页面编号</param>
        /// <param name="Name">页面名称</param>
        /// <param name="ScrollingMode">滚动方式</param>
        /// <param name="SwitchingTime">切换时间</param>
        /// <param name="address1">地址一</param>
        /// <param name="address2">地址二/param>
        /// <param name="address3">地址三</param>
        /// <param name="address4">地址四</param>
        /// <param name="address5">地址五</param>
        /// <param name="address6">地址六</param>
        /// <param name="address7">地址七</param>
        /// <param name="address8">地址八/param>
        /// <param name="address9">地址九</param>
        /// <param name="address10">地址十</param>
        /// <returns></returns>
        public string SetPageRotation(string ID, string Name, string ScrollingMode,string SwitchingTime,string State, string URL1 = null, string URL2 = null, string URL3 = null, string URL4 = null, string URL5 = null, string URL6 = null, string URL7 = null, string URL8 = null, string URL9 = null, string URL10 = null)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(ScrollingMode) || string.IsNullOrEmpty(SwitchingTime))
                        throw new Exception("页面名称、滚动方式、切换时间不能为空！");
                    string[] str = new string[10];
                    if (!string.IsNullOrEmpty(URL1))
                        str[0] = URL1;
                    if (!string.IsNullOrEmpty(URL2))
                        str[1] = URL2;
                    if (!string.IsNullOrEmpty(URL3))
                        str[2] = URL3;
                    if (!string.IsNullOrEmpty(URL4))
                        str[3] = URL4;
                    if (!string.IsNullOrEmpty(URL5))
                        str[4] = URL5;
                    if (!string.IsNullOrEmpty(URL6))
                        str[5] = URL6;
                    if (!string.IsNullOrEmpty(URL7))
                        str[6] = URL7;
                    if (!string.IsNullOrEmpty(URL8))
                        str[7] = URL8;
                    if (!string.IsNullOrEmpty(URL9))
                        str[8] = URL9;
                    if (!string.IsNullOrEmpty(URL10))
                        str[9] = URL10;
                    if (!string.IsNullOrEmpty(ID))
                    {
                        var spdData = db.Queryable<SysPageDisplay>().Where(x => x.ID == ID).Single();
                        spdData.Name = Name;
                        spdData.ScrollingMode = ScrollingMode;
                        spdData.SwitchingTime = SwitchingTime;
                        spdData.State = int.Parse(State);
                        if (db.Updateable(spdData).ExecuteCommand() < 1)
                            throw new Exception("修改轮播信息失败，请重新提交！");
                        var spsList = db.Queryable<SysPageScrolling>().OrderBy(x => x.ID).Where(x => x.PID == ID).ToList();
                        int i = 0;
                        foreach (var item in spsList)
                        {
                            item.URL = str[i];
                            if (db.Updateable(item).UpdateColumns(x => x.URL).ExecuteCommand() < 1)
                                throw new Exception("更改轮播详细地址失败，请重新提交！");
                            i++;
                        }
                    }
                    else
                    {
                        SysPageDisplay spd = new SysPageDisplay
                        {
                            ID = DataHelper.OrderNumber("Rotation").Object.ToString(),
                            Name = Name,
                            ScrollingMode = ScrollingMode,
                            SwitchingTime = SwitchingTime,
                            State = int.Parse(State),
                            CreateTime = DateTime.Now,
                            CreateUser = ThisUser.Uid
                        };
                        if (db.Insertable(spd).ExecuteCommand() < 1)
                            throw new Exception("轮播数据生成失败，请重新提交！");
                        for (int i = 0; i < str.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(str[i]))
                            {
                                SysPageScrolling sps = new SysPageScrolling
                                {
                                    ID = DataHelper.OrderNumber("RotationDetails").Object.ToString(),
                                    Name = "",
                                    PID = spd.ID,
                                    URL = str[i],
                                    State = int.Parse(State),
                                    CreateTime = DateTime.Now,
                                    CreateUser = ThisUser.Uid
                                };
                                if (db.Insertable(sps).ExecuteCommand() < 1) throw new Exception("生成轮播详细失败！");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除轮播主表及与之相关关联的子表
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public string DeletePageRotation(string ID)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (db.Deleteable<SysPageDisplay>().Where(x => x.ID == ID).ExecuteCommand() < 1)
                        throw new Exception("删除轮播数据失败，请重新提交！");
                    if (db.Deleteable<SysPageScrolling>().Where(x => x.PID == ID).ExecuteCommand() < 1)
                        throw new Exception("删除轮播详细失败！");
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }
    }


    public class RotationModel
    {
        //页面编号
        public string ID { get; set; }

        //页面名称
        public string Name { get; set; }

        //滚动方式
        public string ScrollingMode { get; set; }

        //切换时间
        public string SwitchingTime { get; set; }

        //状态
        public int State { get; set; }

        //创建时间
        public DateTime CreateTime { get; set; }

        //创建人
        public string CreateUser { get; set; }

        //页面地址
        public string URL1 { get; set; }
        public string URL2 { get; set; }
        public string URL3 { get; set; }
        public string URL4 { get; set; }
        public string URL5 { get; set; }
        public string URL6 { get; set; }
        public string URL7 { get; set; }
        public string URL8 { get; set; }
        public string URL9 { get; set; }
        public string URL10 { get; set; }
    }
}
