﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBasicApi.Report.DataApi
{
    public class SystemDeleteApiController : ControllerExc
    {
        /// <summary>
        /// 删除表格页面管理
        /// </summary>
        /// <param name="ActionID"></param>
        /// <returns></returns>
        public string DeleteAutoView(string ActionID)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (db.Deleteable<SysAutoView>().Where(x => x.ActionID == ActionID).ExecuteCommand() < 1)
                        throw new Exception("删除表格页面管理失败，请重新提交！");
                    if (db.Queryable<SysViewColunms>().Where(x => x.ActionID == ActionID).ToList().Count > 0)
                        if (db.Deleteable<SysViewColunms>().Where(x => x.ActionID == ActionID).ExecuteCommand() < 1)
                            throw new Exception("删除智能页面数据失败，请联系管理人员！");
                    if (db.Queryable<UViewColunms>().Where(x => x.ActionID == ActionID).ToList().Count > 0)
                        if (db.Deleteable<UViewColunms>().Where(x => x.ActionID == ActionID).ExecuteCommand() < 1)
                            throw new Exception("删除个性页面数据失败，请联系管理人员！");
                    if (db.Queryable<SysAutoMTables>().Where(x => x.ActionID == ActionID).ToList().Count > 0)
                        if (db.Deleteable<SysAutoMTables>().Where(x => x.ActionID == ActionID).ExecuteCommand() < 1)
                            throw new Exception("删除自动多表格页面数据失败，请联系管理人员！");
                    var toolList = db.Queryable<SysToolBar>().Where(x => x.ActionID == ActionID).ToList();
                    if (toolList.Count>0)
                    {
                        foreach (var item in toolList)
                        {
                            if (db.Deleteable<SysToolBarFunction>().Where(x => x.ToolBarID == item.ToolBarID).ExecuteCommand() < 1)
                                throw new Exception("删除工具事件失败，请联系管理人员！");
                        }
                        if (db.Deleteable<SysToolBar>().Where(x => x.ActionID == ActionID).ExecuteCommand() < 1)
                            throw new Exception("删除工具管理失败，请联系管理人员！");
                    }
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除组织机构
        /// </summary>
        /// <param name="OrgAttrId"></param>
        /// <returns></returns>
        public string DeleteOrganization(string OrgAttrId)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (db.Deleteable<SysOrganization>().Where(x => x.OrgAttrId == OrgAttrId).ExecuteCommand() < 1)
                        throw new Exception("删除组织机构失败，请重新提交！");

                    //查询当前组织机构所属用户
                    var userList = db.Queryable<SysUserInfo>().Where(x => x.OrgAttrId.Contains(OrgAttrId)).ToList();
                    foreach (var item in userList)
                    {
                        //一个用户有多个组织机构，以逗号分割
                        string[] orgArray = item.OrgAttrId.Split(',');
                        item.OrgAttrId = "";
                        //循环判断，把当前组织机构从用户组织机构中删除，并重新赋值
                        for (int i = 0; i < orgArray.Length; i++)
                        {
                            if (orgArray[i]!=OrgAttrId)
                                item.OrgAttrId = item.OrgAttrId + "," + orgArray[i];
                        }
                        if (!string.IsNullOrEmpty(item.OrgAttrId))
                            //截取字符串
                            item.OrgAttrId = item.OrgAttrId.Remove(0, 1);
                        if (db.Updateable(item).UpdateColumns(x => x.OrgAttrId).ExecuteCommand() < 1)
                            throw new Exception("修改用户组织机构失败，请联系管理人员！");
                    }

                    //查询并修改用户角色
                    var roleList = db.Queryable<SysUserRole>().Where(x => x.OrgAttrId == OrgAttrId).ToList();
                    if (roleList.Count>0)
                        if (db.Updateable<SysUserRole>().SetColumns(x => x.OrgAttrId == "").Where(x => x.OrgAttrId == OrgAttrId).ExecuteCommand() < 1)
                            throw new Exception("修改用户角色失败，请联系管理人员！");

                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除用户角色
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string DeleteUserRole(string Id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {

                    if (db.Deleteable<SysUserRole>().Where(x => x.Id == Id).ExecuteCommand() < 1)
                        throw new Exception("删除用户角色失败，请重新提交！");
                    var roleList = db.Queryable<SysUserInfo>().Where(x => x.RoleId == Id).ToList();
                    if (roleList.Count>0)
                        if (db.Updateable<SysUserInfo>().SetColumns(x => x.RoleId == "").Where(x => x.RoleId == Id).ExecuteCommand()<1)
                            throw new Exception("更新用户角色失败，请联系管理人员！");
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除工具管理
        /// </summary>
        /// <param name="ToolBarID"></param>
        /// <returns></returns>
        public string DeleteToolBar(string ToolBarID)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (db.Deleteable<SysToolBar>().Where(x => x.ToolBarID == ToolBarID).ExecuteCommand() < 1)
                        throw new Exception("删除工具管理失败，请重新提交！");
                    if (db.Queryable<SysToolBarFunction>().Where(x => x.ToolBarID == ToolBarID).ToList().Count > 0)
                        if (db.Deleteable<SysToolBarFunction>().Where(x => x.ToolBarID == ToolBarID).ExecuteCommand() < 1)
                            throw new Exception("删除工具事件失败，请联系管理人员！");
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除参数模板
        /// </summary>
        /// <param name="TempID"></param>
        /// <returns></returns>
        public string DeleteParameter(string TempID)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var paraData = db.Queryable<SysParameter>().Where(x => x.TempID == TempID).Single();
                    if (db.Deleteable<SysParameter>().Where(x => x.TempID == TempID).ExecuteCommand() < 1)
                        throw new Exception("删除参数模板失败，请重新提交！");
                    var tempList = db.Queryable<SysParameterTemp>().Where(x => x.GroupID == paraData.TempContext).ToList();
                    if (tempList.Count>0)
                        if (db.Deleteable<SysParameterTemp>().Where(x => x.GroupID == paraData.TempContext).ExecuteCommand() < 1)
                            throw new Exception("删除参数模板项失败，请联系管理人员！");
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除数据导入模板
        /// </summary>
        /// <param name="EimportId"></param>
        /// <returns></returns>
        public string DeleteImport(string EimportId)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (db.Deleteable<SysExcleImport>().Where(x => x.EimportId == EimportId).ExecuteCommand() < 1)
                        throw new Exception("删除数据导入模板失败，请重新提交！");
                    var importList = db.Queryable<SysExcleImportCol>().Where(x => x.EimportId == EimportId).ToList();
                    if (importList.Count > 0)
                        if (db.Deleteable<SysExcleImportCol>().Where(x => x.EimportId == EimportId).ExecuteCommand() < 1)
                            throw new Exception("删除导入模板项失败，请联系管理人员！");
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 删除多语言种类管理
        /// </summary>
        /// <param name="LanguageID"></param>
        /// <returns></returns>
        public string DeleteLanguage(string LanguageID)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (db.Deleteable<SysMultiLanguage>().Where(x => x.LanguageID == LanguageID).ExecuteCommand() < 1)
                        throw new Exception("删除语言种类管理失败，请重新提交！");
                    var languageList = db.Queryable<SysLanguage>().Where(x => x.LanguageType == LanguageID).ToList();
                    if (languageList.Count > 0)
                        if (db.Deleteable<SysLanguage>().Where(x => x.LanguageType == LanguageID).ExecuteCommand() < 1)
                            throw new Exception("删除多语言管理失败，请联系管理人员！");
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

    }
}
