﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class WelComeApiController : ControllerExc
    {

        public string DataSum()
        {
            var result = new Result();
            try
            {
                var sql = @"select '终端数' name,count(1) count from SysDevice
                            UNION
                            select '用户数' name,count(1) count from SysUserInfo
                            UNION
                            select '文件数' name,count(1) count from SysFileManager
                            UNION
                            select '文库数' name,count(1) count from UknowledgeBase
                            UNION
                            select '公告数' name,count(1) count from SysNotice
                            UNION
                            select '任务数' name,count(1) count from SysScheduledTask ";
                result = new DataBaseHelper().OnlineSql(sql);
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        public string FileManager()
        {
            var result = new Result();
            try
            {
                var sql = @"select FileType name,count(1)/(select count(1)from SysFileManager)*100  value from SysFileManager GROUP BY FileType  ORDER BY value desc  LIMIT   5; ";
                result = new DataBaseHelper().OnlineSql(sql);
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        public string ServerInfo()
        {
            var result = new Result();
            try
            {
                var machine = new MachineCode();
                var list = new Dictionary<string, string>();
                var disk = machine.GetHardDiskInfoByName();
                list.Add("disk", (double.Parse(disk.FreeSpace) / double.Parse(disk.TotalSpace) * 100).ToString());
                list.Add("ram", machine.GetMemoryPercent().ToString());
                result.Object = list;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        public string ApprAnalysis()
        {
            var result = new Result();
            try
            {
                var startSql = $@"select count(1) num from (SELECT DISTINCT ApprovalID FROM	SysFlowApprovalDetail WHERE	Reviewer = '{ThisUser.Uid}') t;";
                var apprSql = $@"select count(1) num from (SELECT DISTINCT ApprovalID  FROM	SysFlowApprovalDetail WHERE	Reviewer = '{ThisUser.Uid}') t;";
                var rebutSql = $@"select count(1) num from (SELECT DISTINCT ApprovalID FROM	SysFlowApprovalDetail WHERE	Reviewer = '{ThisUser.Uid}' AND State='unsigned') t;";
                var startpre = $@"SELECT round(SUM(TIMESTAMPDIFF(HOUR, SysFlowApproval.CreateDate, SysFlowApprovalDetail.ApprovalDate))/count(1),1) num FROM SysFlowApproval  INNER JOIN SysFlowApprovalDetail ON SysFlowApprovalDetail.ApprovalID=SysFlowApproval.ID WHERE  SysFlowApproval.State in ('pass') and 	SysFlowApproval.Sponsor = '{ThisUser.Uid}' ;";
                var apprpre = $@"SELECT round(SUM(TIMESTAMPDIFF(HOUR, SysFlowApproval.CreateDate, SysFlowApprovalDetail.ApprovalDate))/count(1),1) num FROM SysFlowApproval  INNER JOIN SysFlowApprovalDetail ON SysFlowApprovalDetail.ApprovalID=SysFlowApproval.ID WHERE  SysFlowApproval.State in ('pass') and 	SysFlowApprovalDetail.Reviewer = '{ThisUser.Uid}' ;";
                var rebutpre = $@"SELECT round(SUM(TIMESTAMPDIFF(HOUR, SysFlowApproval.CreateDate, SysFlowApprovalDetail.ApprovalDate))/count(1),1) num FROM SysFlowApproval   INNER JOIN SysFlowApprovalDetail ON SysFlowApprovalDetail.ApprovalID=SysFlowApproval.ID WHERE  SysFlowApproval.State in ('unsigned') and 	SysFlowApproval.Sponsor = '{ThisUser.Uid}' ;";

                var data = new Dictionary<string, string>();
                data.Add("start", ((DataTable)new DataBaseHelper().OnlineSql(startSql).Object).Rows[0]["num"].ToString());
                data.Add("appr", ((DataTable)new DataBaseHelper().OnlineSql(apprSql).Object).Rows[0]["num"].ToString());
                data.Add("rebut", ((DataTable)new DataBaseHelper().OnlineSql(rebutSql).Object).Rows[0]["num"].ToString());
                data.Add("startpre", ((DataTable)new DataBaseHelper().OnlineSql(startpre).Object).Rows[0]["num"].ToString());
                data.Add("apprpre", ((DataTable)new DataBaseHelper().OnlineSql(apprpre).Object).Rows[0]["num"].ToString());
                data.Add("rebutpre", ((DataTable)new DataBaseHelper().OnlineSql(rebutpre).Object).Rows[0]["num"].ToString());

                result.Object = data;
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }


    }
}
