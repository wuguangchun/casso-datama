﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class CostormApiController : ControllerExc
    {

        /// <summary>
        /// 系统参数获取（系统参数模板）
        /// </summary>
        /// <param name="tempId"></param>
        /// <returns></returns>
        public string GetTempPara(string tempId)
        {
            var result = new Result();
            try
            {
                var paraList = DataBaseHelper.GetTempPara(tempId, ThisUser, false);
                result.State = ResultState.Success;
                result.Message = "请求成功";
                result.Object = paraList;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}----{tempId}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 根据用户角色获取功能工具
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public string GetRoleFunction(string roleId)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var function = db.Queryable<SysJurisdiction>().Where(x => x.Roleid == roleId).ToList();
                    result.Object = function.ConvertAll(x => x.ToolBarID).Distinct();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}----{roleId}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 角色权限保存
        /// </summary>
        /// <returns></returns>
        public string SaveRoleFunction()
        {
            var result = new Result();
            try
            {
                var roleID = Request["roleID"];
                var functionid = Request.Form.AllKeys.ToList();
                functionid.Remove("roleID");

                using (var db = new DataBaseHelper().SugarClient())
                {
                    db.Deleteable<SysJurisdiction>().Where(x => x.Roleid == roleID).ExecuteCommand();

                    foreach (var item in functionid)
                    {
                        var toolbar = db.Queryable<SysToolBar>().Where(x => x.ToolBarID == item).Single();
                        var jurisdiction = new SysJurisdiction
                        {
                            ActionID = toolbar.ActionID,
                            ToolBarID = toolbar.ToolBarID,
                            Roleid = roleID,
                            JurisdictionID = DataHelper.GenerateOrderNumber()
                        };
                        db.Insertable(jurisdiction).ExecuteCommand();

                    }
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 生成页面数据
        /// </summary>
        /// <returns></returns>
        public string GeneratePageDdata(string view)
        {
            var result = new Result();
            try
            {
                var list = JsonConvert.DeserializeObject<List<SysAutoView>>(view);


                using (var dbdefault = new DataBaseHelper().SugarClient())
                {
                    foreach (var item in list)
                    {
                        using (var db = new DataBaseHelper().SugarClient(item.DataBase))
                        {
                            var mysql = $@"SELEC table_name tablename,TCOLUMN_NAME fild,COLUMN_TYPE fytpe,DATA_TYPE dtype,IS_NULLABLE isnull,COLUMN_DEFAULT defaultval,COLUMN_COMMENT remarks FROM INFORMATION_SCHEMA.COLUMNS";

                            var mssql = $@"select sysobjects.name tablename, sys.all_columns.name fild,'varchar' dtype,is_nullable,'' defaultval,'' remarks from sys.all_columns 
                                    INNER JOIN sysobjects on sysobjects.id= sys.all_columns.object_id";

                            result = new DataBaseHelper().OnlineSql($"select * from vtablecolunm where tablename='{item.DBTable}'", true, item.DataBase);

                            var data = (DataTable)result.Object;

                            foreach (DataRow row in data.Rows)
                            {
                                var colRow = dbdefault.Queryable<SysViewColunms>().Where(x => x.ActionID == item.ActionID && x.Field == row.Field<string>("fild")).ToList();
                                if (colRow.Count > 0)
                                {
                                    continue;
                                }

                                var col = new SysViewColunms
                                {
                                    ViewColunmId = Guid.NewGuid().ToString(),
                                    ActionID = item.ActionID,
                                    Field = row.Field<string>("fild"),
                                    Title = row.Field<string>("remarks"),
                                    Sorting = 10,
                                    ColType = "normal",
                                    Hide = "false",
                                    Sort = "false",
                                    Unresize = "false",
                                    TotalRow = "false",
                                    Edit = "false",
                                    ReadOnly = 2,
                                    IsQuery = "false",
                                    EditType = "text"
                                };

                                if (!string.IsNullOrEmpty(col.Title) && col.Title.Length > 0)
                                    col.MinWidth = $"{col.Title.Length * 30}";
                                dbdefault.Saveable(col).ExecuteCommand();
                            }
                        }

                    }
                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 生成页面数据
        /// </summary>
        /// <returns></returns>
        public string GenerateMPageDdata(string view)
        {
            var result = new Result();
            try
            {
                var list = JsonConvert.DeserializeObject<List<SysAutoMTables>>(view);


                using (var dbdefault = new DataBaseHelper().SugarClient())
                {
                    foreach (var item in list)
                    {
                        using (var db = new DataBaseHelper().SugarClient(item.DataBase))
                        {
                            var mysql = $@"SELEC table_name tablename,TCOLUMN_NAME fild,COLUMN_TYPE fytpe,DATA_TYPE dtype,IS_NULLABLE isnull,COLUMN_DEFAULT defaultval,COLUMN_COMMENT remarks FROM INFORMATION_SCHEMA.COLUMNS";

                            var mssql = $@"select sysobjects.name tablename, sys.all_columns.name fild,'varchar' dtype,is_nullable,'' defaultval,'' remarks from sys.all_columns 
                                    INNER JOIN sysobjects on sysobjects.id= sys.all_columns.object_id";

                            result = new DataBaseHelper().OnlineSql($"select * from Vtablecolunm where tablename='{item.DBTable}'", true, item.DataBase);

                            var data = (DataTable)result.Object;

                            foreach (DataRow row in data.Rows)
                            {
                                var colRow = dbdefault.Queryable<SysViewColunms>().Where(x => x.ActionID == item.MactionID && x.Field == row.Field<string>("fild")).ToList();
                                if (colRow.Count > 0)
                                {
                                    continue;
                                }

                                var col = new SysViewColunms
                                {
                                    ViewColunmId = Guid.NewGuid().ToString(),
                                    ActionID = item.MactionID,
                                    Field = row.Field<string>("fild"),
                                    Title = row.Field<string>("remarks"),
                                    Sorting = 10,
                                    ColType = "normal",
                                    Hide = "false",
                                    Sort = "false",
                                    Unresize = "false",
                                    TotalRow = "false",
                                    Edit = "false",
                                    ReadOnly = 2,
                                    IsQuery = "false",
                                    EditType = "text"
                                };

                                if (!string.IsNullOrEmpty(col.Title) && col.Title.Length > 0)
                                    col.MinWidth = $"{col.Title.Length * 30}";
                                dbdefault.Saveable(col).ExecuteCommand();
                            }
                        }

                    }
                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 文件删除（状态删除）
        /// </summary>
        /// <param name="fileJson"></param>
        /// <returns></returns>
        public string DeleteFile(string id)
        {
            var result = new Result();
            try
            {

                using (var db = new DataBaseHelper().SugarClient())
                {
                    var file = db.Queryable<SysFileManager>().Where(x => x.ID == id).Single();
                    file.State = 2;
                    db.Updateable(file).ExecuteCommand();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 生成自动视图工具（增删改查）
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        public string GeneratePageTool(string view)
        {
            var result = new Result();
            try
            {
                var list = JsonConvert.DeserializeObject<List<SysAutoView>>(view);


                using (var db = new DataBaseHelper().SugarClient())
                {
                    foreach (var item in list)
                    {
                        if (db.Queryable<SysToolBar>().Where(x => x.ActionID == item.ActionID && x.Event == "add").ToList().Count < 1)
                        {

                            var sysTool = new SysToolBar
                            {
                                ToolBarID = DataHelper.GenerateOrderNumber(),
                                ActionID = item.ActionID,
                                ToolBarName = "新",
                                ToolType = "ToolBar",
                                Sort = 10,
                                Event = "add",
                                Class = "layui-btn layui-btn-xs"
                            };

                            var toolFunction = new SysToolBarFunction
                            {
                                ToolBarID = sysTool.ToolBarID,
                                Function = @"layer.open({       title: '    ',       type: 1,       shift: 1,       resize:false,   offset: 'r',area: [document.body.offsetWidth > 700 ? '600px' : '100%', '100%'],        zIndex: 900,        content: $('#form_edit'),        cancel: function (index, layero) {                    $(':reset').click();                    $('#form_edit').css('display', 'none'); }});",
                                CreateDate = DateTime.Now,
                                CreateUser = ThisUser.Uid
                            };
                            db.Insertable(sysTool).ExecuteCommand();
                            db.Insertable(toolFunction).ExecuteCommand();

                        }
                        if (db.Queryable<SysToolBar>().Where(x => x.ActionID == item.ActionID && x.Event == "serach").ToList().Count < 1)
                        {
                            var sysTool = new SysToolBar
                            {
                                ToolBarID = DataHelper.GenerateOrderNumber(),
                                ActionID = item.ActionID,
                                ToolBarName = "查",
                                ToolType = "ToolBar",
                                Sort = 0,
                                Event = "serach",
                                Class = "layui-btn layui-btn-sm"
                            };

                            var toolFunction = new SysToolBarFunction
                            {
                                ToolBarID = sysTool.ToolBarID,
                                Function = @"dynamicCondition.getInstance('complexInstance-json').open();",
                                CreateDate = DateTime.Now,
                                CreateUser = ThisUser.Uid
                            };
                            db.Insertable(sysTool).ExecuteCommand();
                            db.Insertable(toolFunction).ExecuteCommand();
                        }
                        if (db.Queryable<SysToolBar>().Where(x => x.ActionID == item.ActionID && x.Event == "delArray").ToList().Count < 1)
                        {
                            var sysTool = new SysToolBar
                            {
                                ToolBarID = DataHelper.GenerateOrderNumber(),
                                ActionID = item.ActionID,
                                ToolBarName = "批量删",
                                ToolType = "ToolBar",
                                Sort = 20,
                                Event = "delArray",
                                Class = "layui-btn layui-btn-danger layui-btn-sm"
                            };

                            var toolFunction = new SysToolBarFunction
                            {
                                ToolBarID = sysTool.ToolBarID,
                                Function = @"delData(checkStatus,true);",
                                CreateDate = DateTime.Now,
                                CreateUser = ThisUser.Uid
                            };
                            db.Insertable(sysTool).ExecuteCommand();
                            db.Insertable(toolFunction).ExecuteCommand();
                        }

                        if (db.Queryable<SysToolBar>().Where(x => x.ActionID == item.ActionID && x.Event == "del").ToList().Count < 1)
                        {
                            var sysTool = new SysToolBar
                            {
                                ToolBarID = DataHelper.GenerateOrderNumber(),
                                ActionID = item.ActionID,
                                ToolBarName = "删",
                                ToolType = "Tool",
                                Sort = 10,
                                Event = "del",
                                Class = "layui-btn layui-btn-danger layui-btn-sm"
                            };

                            var toolFunction = new SysToolBarFunction
                            {
                                ToolBarID = sysTool.ToolBarID,
                                Function = @"delData(obj.data);",
                                CreateDate = DateTime.Now,
                                CreateUser = ThisUser.Uid
                            };
                            db.Insertable(sysTool).ExecuteCommand();
                            db.Insertable(toolFunction).ExecuteCommand();
                        }
                        if (db.Queryable<SysToolBar>().Where(x => x.ActionID == item.ActionID && x.Event == "edit").ToList().Count < 1)
                        {
                            var sysTool = new SysToolBar
                            {
                                ToolBarID = DataHelper.GenerateOrderNumber(),
                                ActionID = item.ActionID,
                                ToolBarName = "改",
                                ToolType = "Tool",
                                Sort = 20,
                                Event = "edit",
                                Class = "layui-btn layui-btn-danger layui-btn-sm"
                            };

                            var toolFunction = new SysToolBarFunction
                            {
                                ToolBarID = sysTool.ToolBarID,
                                Function = @"edit(obj.data);",
                                CreateDate = DateTime.Now,
                                CreateUser = ThisUser.Uid
                            };
                            db.Insertable(sysTool).ExecuteCommand();
                            db.Insertable(toolFunction).ExecuteCommand();
                        }
                    }
                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

    }
}
