﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class SystemApiController : Controllerm
    {
        /// <summary>
        /// 设置系统默认语言
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public string SystemDefaultLanguage(string language)
        {
            var result = new Result();
            try
            {
                var languageClass = JsonConvert.DeserializeObject<List<SysMultiLanguage>>(language).FirstOrDefault();

                using (var db = new DataBaseHelper().SugarClient())
                {
                    db.Updateable<SysMultiLanguage>(new { DefaultUse = "Y" })
                       .Where(it => it.LanguageID == languageClass.LanguageID).ExecuteCommand();
                    db.Updateable<SysMultiLanguage>(new { DefaultUse = "N" })
                       .Where(it => it.LanguageID != languageClass.LanguageID).ExecuteCommand();
                }
                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004").First().LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = $"{ex.Message}--({language})";
                new LogNetDataBase().SysImpleLog(typeof(SystemApiController).FullName, $"设置系统默认语言-{language}", ex.Message, "");
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        ///系统初始化接口
        /// </summary>
        /// <returns></returns>
        public string IntallSystem()
        {
            var result = new Result();
            try
            {
                var dBPwd = Request["DBPwd"].ToString().Trim();
                var dBUser = Request["DBUser"].ToString().Trim();
                var dBName = Request["DBName"].ToString().Trim();
                var dBHost = Request["DBHost"].ToString().Trim();
                var dbType = Request["DbType"].ToString().Trim();
                var port = Request["port"].ToString().Trim();

                //检测用户填写的内容是否正确
                if (string.IsNullOrEmpty(dBPwd) || string.IsNullOrEmpty(dBUser) || string.IsNullOrEmpty(dBName)
                    || string.IsNullOrEmpty(dBHost) || string.IsNullOrEmpty(dbType))
                {
                    throw new Exception("信息填写异常");
                }
                else
                {
                    string connectStr = string.Empty;
                    //验证数据库链接是否正确
                    switch (dbType)
                    {
                        case "MySql":
                            connectStr = $"server={dBHost};port={port};uid={dBUser};pwd={dBPwd};database={dBName}";
                            break;
                        default:
                            connectStr = $"server={dBHost};uid={dBUser};;pwd={dBPwd};database={dBName}";
                            break;
                    }

                    //将参数写入配置文件中
                    ConfigHelper.WriteOtherConfigFile("DbType", dbType);
                    ConfigHelper.WriteOtherConfigFile("ConnectStr", connectStr);

                }

                result.State = ResultState.Success;
                result.Message = "配置更新完成";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 反向生成数据库表
        /// </summary>
        /// <returns></returns>
        public string CreateDataBase()
        {
            var result = new Result();
            try
            {
                return new DataBaseHelper().CreateDataBase().ToString();
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 数据库表数据初始化
        /// </summary>
        /// <returns></returns>
        public string InitData()
        {
            var result = new Result();
            try
            {
                var sql = new FileHelper().ReadFile($@"/Init/{ConfigHelper.ReadOtherConfigFile("DbType")}/init.sql", System.Text.Encoding.UTF8);
                var list = sql.Split('\n');
                foreach (var sqlLine in list)
                {
                    try
                    {
                        new DataBaseHelper().OnlineExecSql(sqlLine, false);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }

                ConfigHelper.WriteOtherConfigFile("DefultAction", "Administrator|Login");
                ConfigHelper.WriteOtherConfigFile("InstallLock", "false");
                HttpRuntime.UnloadAppDomain();
                result.State = ResultState.Success;
                result.Message = "数据初始化完成";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(typeof(SystemApiController).FullName, "数据库表数据初始化", ex.Message, "");

            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 定时任务调度接口
        /// </summary>
        /// <returns></returns>
        public string ScheduledTaskAuto()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    //获取开启有效的任务列表
                    var tasks = db.Queryable<SysScheduledTask>()
                        .Where(x => x.TaskState == 1).ToList();

                    foreach (var item in tasks)
                    {
                        var exec = false;
                        switch (item.Efficiency)
                        {
                            case "year":
                                exec = item.ExecTime.AddYears(item.Role) <= DateTime.Now;
                                break;
                            case "month":
                                exec = item.ExecTime.AddMonths(item.Role) <= DateTime.Now;
                                break;
                            case "day":
                                exec = item.ExecTime.AddDays(item.Role) <= DateTime.Now;
                                break;
                            case "hour":
                                exec = item.ExecTime.AddHours(item.Role) <= DateTime.Now;
                                break;
                            case "minute":
                                exec = item.ExecTime.AddMinutes(item.Role) <= DateTime.Now;
                                break;
                            case "second":
                                exec = item.ExecTime.AddSeconds(item.Role) <= DateTime.Now;
                                break;
                            case "week":
                                exec = item.ExecTime.AddDays(item.Role * 7).DayOfWeek <= DateTime.Now.DayOfWeek;
                                break;
                        }

                        if (exec)
                        {
                            var webResult = string.Empty;
                            new PushWebHelper().PushToGet($"{LocalHost}{item.TaskUri}", ref webResult, Encoding.UTF8);
                            item.Result = webResult;
                        }
                        item.ExecTime = DateTime.Now;
                        db.Updateable(item).ExecuteCommand();
                    }

                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 新设备绑定请求接口
        /// </summary>
        /// <returns></returns>
        public string NewDevice()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var id = Guid.NewGuid().ToString();
                    var device = new SysDevice
                    {
                        DeviceId = id,
                        DeviceName = "未知设备",
                        DeviceType = "GeneralStation",
                        State = 1
                    };

                    db.Saveable(device).ExecuteCommand();

                    result.Object = id;
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 系统帮助文档接口
        /// </summary>
        /// <returns></returns>
        public string HelperDoc(string type)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    result.Object = db.Queryable<SysConfig>()
                        .Where(x => x.CfgCode.StartsWith($"{type}-"))
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();
        }

        /// <summary>
        /// 获取参数接口
        /// </summary>
        /// <param name="tempID"></param>
        /// <returns></returns>
        public string GetDBPara(string tempID)
        {

            var result = new Result();
            try
            {
                result.Object = DataBaseHelper.GetTempPara(tempID, null);
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();

        }

        /// <summary>
        ///根据视图列ID获取模板项
        /// </summary>
        /// <param name="colID"></param>
        /// <returns></returns>
        public string GetTempByColID(string colID)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {

                    var col = db.Queryable<SysViewColunms>()
                         .Where(x => x.ViewColunmId == colID)
                         .ToList().FirstOrDefault();
                    if (col == null)
                        throw new Exception("该列数据不存在，请刷新页面后重试！");

                    return GetDBPara(col.Templet);
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();

        }

        /// <summary>
        /// 移动端检测升级接口
        /// </summary>
        /// <returns></returns>
        public string AppOnlineUpdate()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (string.IsNullOrEmpty(Request["version"]) || string.IsNullOrEmpty(Request["platform"]))
                    {
                        throw new Exception("请求不完整，缺少必要参数！");
                    }

                    var query = db.Queryable<MobileVersion>()
                        .Where(x => x.platform == Request["platform"] && x.CreateDate <= DateTime.Now);
                    query.OrderBy("CreateDate desc");
                    var list = query.ToList();
                    if (list.Count > 0)
                    {
                        var version = list.First();
                        if (int.Parse(Request["version"].Replace(".", "")) < int.Parse(version.version.Replace(".", "")))
                        {
                            //需要升级
                            version.update_flag = 1;
                        }
                        else
                        {
                            //不需要升级
                            version.update_flag = 0;
                        }
                        result.Object = version;
                    }
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 获取数据源属性
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetViewCol(string id)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    result.Object = db.Queryable<SysViewColunms>()
                        .Where(x => x.ActionID == id)
                        .ToList();

                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return result.ToString();

        }
    }
}
