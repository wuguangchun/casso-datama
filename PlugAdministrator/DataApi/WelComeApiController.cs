﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class WelComeApiController : Controller
    {

        public string DataSum()
        {
            var result = new Result();
            try
            {
                var sql = @"select '终端数' name,count(1) count from SysDevice
                            UNION
                            select '用户数' name,count(1) count from SysUserInfo
                            UNION
                            select '文件数' name,count(1) count from SysFileManager
                            UNION
                            select '文库数' name,count(1) count from UknowledgeBase
                            UNION
                            select '公告数' name,count(1) count from SysNotice
                            UNION
                            select '任务数' name,count(1) count from SysScheduledTask ";
                result = new DataBaseHelper().OnlineSql(sql);
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        public string FileManager()
        {
            var result = new Result();
            try
            {
                var sql = @"select FileType name,count(1)/(select count(1)from SysFileManager)*100  value from SysFileManager GROUP BY FileType  ORDER BY value desc  LIMIT   5; ";
                result = new DataBaseHelper().OnlineSql(sql);
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }


    }
}
