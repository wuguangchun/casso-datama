﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class TestApiController : Controller
    {

        [HttpGet]
        public string Sendmsg()
        {
            var result = new Result();
            try
            {
                MessageHelper.PushMsgMobile("","这里是主要显示文字","那这里的文字要显示在哪个区域呢？");
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

    }
}
