﻿using LibraryCore.Data;
using LibraryModel.OtherModels;
using LibraryModel.WorkWechatModels;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibraryCore.WorkWeixin
{
    public class WorkWechatHelper
    {
        /// <summary>
        /// 企业微信获取AccessToken方法
        /// </summary>
        /// <returns></returns>
        public Result GetAccessToken(int num = 0)
        {
            var result = new Result();
            try
            {
                string weResultJson = string.Empty;
                switch (num)
                {
                    case 1:
                        new PushWebHelper().PushToGet(WorkWechat.gettoken1, ref weResultJson, Encoding.UTF8); break;
                    case 2:
                        new PushWebHelper().PushToGet(WorkWechat.gettoken2, ref weResultJson, Encoding.UTF8); break;
                    case 3:
                        new PushWebHelper().PushToGet(WorkWechat.gettoken3, ref weResultJson, Encoding.UTF8); break;
                    default:
                        new PushWebHelper().PushToGet(WorkWechat.gettoken, ref weResultJson, Encoding.UTF8);
                        break;
                }

                var weResult = JsonConvert.DeserializeObject<AccessTokenResult>(weResultJson);
                if (weResult.errcode == 0)
                {
                    ConfigHelper.WriteOtherConfigFile("WorkAccessToken" + (num == 0 ? "" : num.ToString()), weResult.access_token);
                    ConfigHelper.WriteOtherConfigFile("WorkAccessTokenValid" + (num == 0 ? "" : num.ToString()), DateTime.Now.AddSeconds(weResult.expires_in).ToString());
                }
                else
                {
                    throw new Exception(weResult.errmsg);
                }

                result.State = ResultState.Success;
                result.Message = weResult.errmsg;
                result.Object = weResult;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog("企业微信获取AccessToken", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}----{num}", null);
            }
            return result;
        }

        /// <summary>
        /// 企业微信获取部门列表方法
        /// </summary>
        /// <returns></returns>
        public List<Department> GetDepartment()
        {
            try
            {
                //获取企业部门
                var weResultJson = string.Empty;
                new PushWebHelper().PushToGet(WorkWechat.department, ref weResultJson, Encoding.UTF8);

                var weResult = JsonConvert.DeserializeObject<DepartmentResult>(weResultJson);
                if (weResult.errcode != 0)
                {
                    throw new Exception(weResult.errmsg);
                }

                return weResult.department;
            }
            catch (Exception ex)
            {
                LogNet.WriteLog("企业微信获取部门列表", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
                return null;
            }
        }

        /// <summary>
        /// 企业微信获取部门列表方法
        /// </summary>
        /// <returns></returns>
        public List<WeUser> GetDepartmentUser(int departmentId)
        {
            try
            {
                //获取企业部门
                var weResultJson = string.Empty;
                new PushWebHelper().PushToGet(
                    $"https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={WorkWechat.AccessToken}&department_id={departmentId}",
                    ref weResultJson, Encoding.UTF8);

                var weResult = JsonConvert.DeserializeObject<WeUserResult>(weResultJson);
                if (weResult.errcode != 0)
                {
                    throw new Exception(weResult.errmsg);
                }

                return weResult.userlist;
            }
            catch (Exception ex)
            {
                LogNet.WriteLog("企业微信获取部门列表", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}----{departmentId}", null);
                return null;
            }
        }

        /// <summary>
        /// 给用户发消息
        /// </summary>
        /// <param name="touser">用户</param>
        /// <param name="toparty">部门</param>
        /// <param name="totag">标签</param>
        /// <param name="msgtype"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public Result SendMessageToUser(string touser, string msgtype, string content)
        {
            var result = new Result();
            try
            {
                string weResultJson = string.Empty;

                var postdata = new WeTextMsg
                {
                    touser = touser,
                    msgtype = msgtype,
                    agentid = int.Parse(ConfigHelper.ReadOtherConfigFile("agentid")),
                    text = new Content { content = content }
                };


                new PushWebHelper().PushToPost(WorkWechat.messageSend, JsonConvert.SerializeObject(postdata), ref weResultJson);
                var weResult = JsonConvert.DeserializeObject<AccessTokenResult>(weResultJson);
                if (weResult.errcode != 0)
                {
                    throw new Exception(weResult.errmsg);
                }

                result.State = ResultState.Success;
                result.Message = weResult.errmsg;
                result.Object = weResult;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog("企业微信获取AccessToken", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}----{touser}--{content}", null);
            }
            return result;
        }
    }
}
