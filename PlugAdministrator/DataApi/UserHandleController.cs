﻿using LibraryCore;
using LibraryCore.Core;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace LibraryBasicApi.Controllers
{
    public class UserHandleController : Controller
    {

        /// <summary>
        /// 用户登陆
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public string UserLogin(string name, string pwd)
        {
            var result = new Result();
            try
            {
                var db = new DataBaseHelper().SugarClient();

                //密码验证
                if (string.IsNullOrEmpty(pwd))
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg011", null).FirstOrDefault()?.LanguageValue);
                }

                var userInfo = db.Queryable<SysUserInfo>().Where(x => x.UserName == name && x.PassWord == new EncryptHelper(ConfigurationSettings.AppSettings["Encrypkey"].Substring(0, 24)).Encrypt(pwd)).Single();

                if (userInfo is null)
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg012", null).FirstOrDefault()?.LanguageValue);
                }

                if (userInfo.Status < 1)
                {
                    throw new Exception("该账户已被冻结，请联系软件维护工程师协助解冻！");
                }
                userInfo.LastTime = DateTime.Now;

                if (string.IsNullOrEmpty(userInfo.AccessToken))
                {
                    userInfo.AccessToken = EncryptHelper.Authorization($@"{userInfo.Uid}{userInfo.PassWord}");
                }

                db.Updateable(userInfo).ExecuteCommand();

                //将用户信息使用系统内核Cookie操作方式存储
                CookieHelper.SaveCookie("WebUser", JsonConvert.SerializeObject(userInfo), Response, 60 * 60 * 30);

                UserMenu();
                result.State = ResultState.Success;
                result.Message = $"{userInfo.Nick}-{new LanguageHelper().GetLanguage("msg013", null).FirstOrDefault()?.LanguageValue}";
                result.Object = userInfo;

                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"用户登录成功，登录IP：{Request.ServerVariables["REMOTE_ADDR"]}", userInfo.Uid);

                //给用户发送消息
                new MqHelper().Publishing(userInfo.Uid, new Result { Message = $"登录成功，{userInfo.Nick }欢迎您！" });
            }
            catch (Exception ex)
            {
                LogNet.WriteLog($"用户登录方法 UserLogin", ex);
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}--{name}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 用户通过令牌登陆
        /// </summary>
        /// <param name="token"></param> 
        /// <returns></returns>
        public string UserLoginByToken()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var token = Request.Headers.Get("token");
                    var clientid = Request.Headers.Get("clientid");

                    //令牌验证
                    if (string.IsNullOrEmpty(token))
                    {
                        throw new Exception(new LanguageHelper().GetLanguage("msg011", null).FirstOrDefault()?.LanguageValue);
                    }

                    var userInfo = db.Queryable<SysUserInfo>().Where(x => x.AccessToken == token).Single();

                    if (userInfo is null)
                    {
                        throw new Exception(new LanguageHelper().GetLanguage("msg012", null).FirstOrDefault()?.LanguageValue);
                    }


                    if (userInfo.Status < 1)
                    {
                        throw new Exception("该账户已被冻结，请联系软件维护工程师协助解冻！");
                    }
                    userInfo.LastTime = DateTime.Now;
                    if (!string.IsNullOrEmpty(clientid))
                    {
                        userInfo.AppClientId = clientid;
                    }
                    db.Updateable(userInfo).ExecuteCommand();

                    //将用户信息使用系统内核Cookie操作方式存储
                    CookieHelper.SaveCookie("WebUser", JsonConvert.SerializeObject(userInfo), Response, 60 * 60 * 30);

                    UserMenu();
                    result.State = ResultState.Success;
                    result.Message = $"{userInfo.Nick}-{new LanguageHelper().GetLanguage("msg013", null).FirstOrDefault()?.LanguageValue}";
                    result.Object = userInfo;

                    new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"用户登录成功，登录IP：{Request.ServerVariables["REMOTE_ADDR"]}", userInfo.Uid);

                    //给用户发送消息
                    new MqHelper().Publishing(userInfo.Uid, new Result { Message = $"用户使用Token登录成功，{userInfo.Nick }欢迎您！" }); 

                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 用户锁屏解锁
        /// </summary>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public string UserUnlock(string pwd)
        {
            var result = new Result();
            try
            {
                string userJson = CookieHelper.ReadCookie("WebUser", Request);
                var user = JsonConvert.DeserializeObject<SysUserInfo>(userJson);
                if (new EncryptHelper(ConfigurationSettings.AppSettings["Encrypkey"].Substring(0, 24)).Encrypt(pwd) == user.PassWord)
                {
                    result.State = ResultState.Success;
                    result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
                }
                else
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg011", null).FirstOrDefault()?.LanguageValue);
                }
            }
            catch (Exception ex)
            {
                LogNet.WriteLog($"用户锁屏解锁方法 UserUnlock", ex);
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// /用户密码修改
        /// </summary>
        /// <returns></returns>
        public string UserEditPwd()
        {
            var result = new Result();
            try
            {
                string pwd = Request["pwd"];
                string npwd = Request["npwd"];
                string npwdt = Request["npwdt"];

                if (npwd != npwdt)
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg014", null).FirstOrDefault()?.LanguageValue);
                }

                string userJson = CookieHelper.ReadCookie("WebUser", Request);
                var user = JsonConvert.DeserializeObject<SysUserInfo>(userJson);

                var pwdo = new EncryptHelper(ConfigurationSettings.AppSettings["Encrypkey"].Substring(0, 24)).Encrypt(pwd);
                if (pwdo == user.PassWord)
                {
                    user.PassWord = new EncryptHelper(ConfigurationSettings.AppSettings["Encrypkey"].Substring(0, 24)).Encrypt(npwd);
                    user.AccessToken = EncryptHelper.Authorization($@"{user.Uid}{user.PassWord}");
                    var db = new DataBaseHelper().SugarClient();
                    var row = db.Updateable(user).UpdateColumns(x => new { x.PassWord, x.AccessToken }).Where(x => x.Uid == user.Uid).ExecuteCommand();
                }
                else
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg015", null).FirstOrDefault()?.LanguageValue);
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                LogNet.WriteLog($"用户修改密码 UserEditPwd", ex);
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 用户登出
        /// </summary>
        /// <returns></returns>
        public string UserOut()
        {
            var result = new Result();
            try
            {
                CookieHelper.DeleteCookie("WebUser", HttpContext);
                CookieHelper.DeleteCookie("WebAdminMenu", HttpContext);

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"用户登出 UserOut", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 用户主菜单
        /// </summary>
        /// <returns></returns>
        public string UserMenu()
        {
            var result = new Result();
            try
            {
                var user = JsonConvert.DeserializeObject<SysUserInfo>(CookieHelper.ReadCookie("WebUser", Request));
                var db = new DataBaseHelper().SugarClient(); ;
                var uRole = SmartData.GetData<SysUserRole>("SysUserRole", new Condition { ConditionFieldVal = "Id", ConditionOptionVal = ConditionOption.equal, ConditionValueVal = new ValueVal { Value = user.RoleId, Text = user.RoleId } }, 0).FirstOrDefault();// db.Queryable<SysUserRole>().Where(x => x. == user.RoleId).Single();
                var action = db.Queryable<SysAction>().Where(x => SqlFunc.ContainsArray(uRole.HavingPage.Split(','), x.Id)).ToList();

                //多语言转换
                action.ForEach(x => x.ActionName = new LanguageHelper().GetLanguage(x.ActionName, user).Count > 0 ? new LanguageHelper().GetLanguage(x.ActionName, user).FirstOrDefault().LanguageValue : x.ActionName);

                var menus = new List<MenuModel>();
                action.RemoveAll(x => x.ShowMenu == "N" || x.Mobile == "Y");
                //生成左侧菜单栏
                foreach (var frist in action.FindAll(x => string.IsNullOrEmpty(x.ParentID)).OrderBy(x => x.Short))
                {
                    var fristMenu = new MenuModel
                    {
                        id = frist.Id,
                        title = frist.ActionName,
                        href = frist.ActionUrl,
                        icon = frist.Icon,
                        spread = false,
                        children = new List<MenuModel>()
                    };

                    foreach (var second in action.FindAll(x => x.ParentID == frist.Id).OrderBy(x => x.Short))
                    {
                        //二级菜单
                        var secondMenu = new MenuModel()
                        {
                            id = second.Id,
                            title = second.ActionName,
                            href = second.ActionUrl,
                            icon = second.Icon,
                            spread = false,
                            children = new List<MenuModel>()
                        };

                        foreach (var third in action.FindAll(x => x.ParentID == second.Id).OrderBy(x => x.Short))
                        {

                            //三级菜单
                            var thirdMenu = new MenuModel
                            {
                                id = third.Id,
                                title = third.ActionName,
                                href = third.ActionUrl,
                                icon = third.Icon,
                                spread = false,
                                children = new List<MenuModel>()
                            };

                            foreach (var fourth in action.FindAll(x => x.ParentID == third.Id).OrderBy(x => x.Short))
                            {

                                //四级菜单
                                var fourthMenu = new MenuModel
                                {
                                    id = fourth.Id,
                                    title = fourth.ActionName,
                                    href = fourth.ActionUrl,
                                    icon = fourth.Icon,
                                    spread = false,
                                };

                                thirdMenu.children.Add(fourthMenu);
                            }
                            secondMenu.children.Add(thirdMenu);
                        }
                        fristMenu.children.Add(secondMenu);
                    }
                    if (fristMenu.children.Count > 0 && fristMenu.children != null)
                    {
                        fristMenu.children.RemoveAll(x => string.IsNullOrEmpty(x.href) && x.children.Count < 1);
                        //CookieHelper.SaveCookie(fristMenu.id, JsonConvert.SerializeObject(fristMenu.children), Response, 60 * 60 * 8);
                        CacheHelper.Insert(fristMenu.id, fristMenu.children);
                        fristMenu.children = null;
                        menus.Add(fristMenu);
                    }
                }

                //将用户信息使用系统内核操作方式存储
                //CookieHelper.SaveCookie("WebAdminMenu", JsonConvert.SerializeObject(menus), Response, 60 * 60 * 8);
                CacheHelper.Insert("WebAdminMenu", menus);

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
                result.Object = menus;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取用户菜单，重刷所有 UserMenu", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 获取子菜单
        /// </summary>
        /// <param name="fristId"></param>
        /// <returns></returns>
        public string UserSecondMenu(string fristId)
        {
            try
            {
                //var menu = CookieHelper.ReadCookie(fristId, Request);
                var menuCache = CacheHelper.Get(fristId);
                if (menuCache == null)
                {
                    UserMenu();
                }
                var menu = CacheHelper.Get(fristId).ToString();
                return menu;
            }
            catch (Exception ex)
            {
                LogNet.WriteLog($"获取用户二级所有菜单， UserMenu", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
                return null;
            }
        }

        /// <summary>
        /// 移动端功能菜单获取
        /// </summary>
        /// <returns></returns>
        public string UserMenuMobile()
        {
            var result = new Result();
            try
            {
                var user = JsonConvert.DeserializeObject<SysUserInfo>(CookieHelper.ReadCookie("WebUser", Request));
                var db = new DataBaseHelper().SugarClient(); ;
                var uRole = SmartData.GetData<SysUserRole>("SysUserRole", new Condition { ConditionFieldVal = "Id", ConditionOptionVal = ConditionOption.equal, ConditionValueVal = new ValueVal { Value = user.RoleId, Text = user.RoleId } }, 0).FirstOrDefault();// db.Queryable<SysUserRole>().Where(x => x. == user.RoleId).Single();
                var action = db.Queryable<SysAction>().Where(x => SqlFunc.ContainsArray(uRole.HavingPage.Split(','), x.Id)).ToList();

                action.RemoveAll(x => x.ShowMenu == "N" || x.Mobile != "Y");

                //多语言转换
                action.ForEach(x => x.ActionName = new LanguageHelper().GetLanguage(x.ActionName, user).Count > 0 ? new LanguageHelper().GetLanguage(x.ActionName, user).FirstOrDefault().LanguageValue : x.ActionName);

                var menus = new List<MenuModel>();

                foreach (var item in action.FindAll(x => string.IsNullOrEmpty(x.ParentID)).OrderBy(x => x.Short))
                {
                    var menu = new MenuModel { id = item.Id, title = item.ActionName, href = item.ActionUrl, icon = item.Icon, name = DataHelper.DataToPinYin(item.ActionName), children = new List<MenuModel>() };

                    foreach (var item1 in action.FindAll(x => x.ParentID == item.Id).OrderBy(x => x.Short))
                    {
                        var menu2 = new MenuModel { id = item1.Id, title = item1.ActionName, href = item1.ActionUrl, icon = item1.Icon, name = DataHelper.DataToPinYin(item1.ActionName), children = new List<MenuModel>() };
                        foreach (var item2 in action.FindAll(x => x.ParentID == item1.Id).OrderBy(x => x.Short))
                        {
                            menu2.children.Add(new MenuModel { id = item2.Id, title = item2.ActionName, href = item2.ActionUrl, icon = item2.Icon, name = DataHelper.DataToPinYin(item2.ActionName), });
                        }

                        menu.children.Add(menu2);

                    }

                    menus.Add(menu);
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
                result.Object = menus;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取移动端用户菜单，重刷所有 UserMenu", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }
            return JsonConvert.SerializeObject(result);
        }


        /// <summary>
        /// 修改信息
        /// </summary>
        /// <returns></returns>
        public string AddOrUpdate()
        {
            var result = new Result();
            try
            {

                var db = new DataBaseHelper().SugarClient(); ;

                var user = new SysUserInfo
                {
                    UserName = Request["UserName"],
                    Nick = Request["Nick"],
                    Telphone = Request["Telphone"],
                    Email = Request["Email"],
                    Sex = int.Parse(Request["Sex"]),
                    RoleId = Request["RoleId"],
                    Status = int.Parse(Request["Status"]),
                    OrgAttrId = Request["OrgAttrId"]
                };
                if (!string.IsNullOrEmpty(Request["PassWord"]))
                {
                    user.PassWord = Request["PassWord"];
                }

                if (string.IsNullOrEmpty(Request["Uid"]))
                {
                    user.Uid = DataHelper.GenerateOrderNumber();
                    user.CreateTime = DateTime.Now;

                    #region 约束

                    if (db.Queryable<SysUserInfo>().Where(x => x.UserName == user.UserName).Single() != null)
                    {
                        throw new Exception("用户名已存在,请更改");
                    }

                    #endregion

                }
                else
                {
                    user.Uid = Request["Uid"];
                }

                db.Saveable(user).UpdateIgnoreColumns(it => it.PassWord).ExecuteReturnEntity();

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"Update [SysUserInfo] Error----{ex.Message}");
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 将系统列信息同步到用户个性设置表中
        /// </summary>
        /// <returns></returns>
        public string SyncViewColunms()
        {

            var result = new Result();
            try
            {
                string userJson = CookieHelper.ReadCookie("WebUser", Request);
                var user = JsonConvert.DeserializeObject<SysUserInfo>(userJson);

                using (var db = new DataBaseHelper().SugarClient())
                {
                    var defaultList = db.Queryable<SysViewColunms>().ToList();
                    var uList = db.Queryable<UViewColunms>().Where(x => x.CreateUser == user.Uid).ToList();

                    var list = defaultList.ConvertAll(x => x.ViewColunmId).Except(uList.ConvertAll(x => x.ViewColunmId));
                    foreach (var item in list)
                    {
                        var defaultv = defaultList.Find(x => x.ViewColunmId == item);
                        UViewColunms uViewColunm = new UViewColunms
                        {
                            UViewColunmId = DataHelper.GenerateOrderNumber(),
                            ViewColunmId = item,
                            CreateUser = user.Uid,
                            ActionID = defaultv.ActionID,
                            Field = defaultv.Field,
                            Title = defaultv.Title,
                            Width = defaultv.Width,
                            MinWidth = defaultv.MinWidth,
                            Sorting = defaultv.Sorting,
                            ColType = defaultv.ColType,
                            Fixed = defaultv.Fixed,
                            Hide = defaultv.Hide,
                            Sort = defaultv.Sort,
                            Unresize = defaultv.Unresize,
                            TotalRow = defaultv.TotalRow,
                            Edit = defaultv.Edit,
                            ReadOnly = defaultv.ReadOnly,
                            Event = defaultv.Event,
                            Style = defaultv.Style,
                            IsQuery = defaultv.IsQuery,
                            EditType = defaultv.EditType,
                            Required = defaultv.Required,
                            Align = defaultv.Align,
                            Templet = defaultv.Templet,
                            TempFormat = defaultv.TempFormat,
                            DefaultVal = defaultv.DefaultVal


                        };
                        db.Insertable(uViewColunm).ExecuteCommand();
                    }
                }
                result.Message = new LanguageHelper().GetLanguage("msg004", user).FirstOrDefault().LanguageValue;
                result.State = ResultState.Success;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.State = ResultState.Error;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);
        }
    }
}