﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibraryBasicApi.DataApi
{
    public class RootApisController : ControllerExc
    {
        /// <summary>
        /// 根据视图ID获取审批流
        /// </summary>
        /// <param name="viewID"></param>
        /// <returns></returns>
        public string GetViewDataByID(string viewID)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var view = db.Queryable<SysFlow>()
                        .Where(x => x.ActionID == viewID)
                        .ToList().FirstOrDefault();
                    if (view == null)
                        throw new Exception("还没有创建过流程哦！");

                    result.Object = view;
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return result.ToString();
        }


    }
}
