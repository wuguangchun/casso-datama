﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LibraryBasicApi.Controllers
{
    public class PublicApiController : ControllerExc
    {
        /// <summary>
        /// 订单明细
        /// </summary>
        /// <param name="djbh"></param>
        /// <returns></returns>
        public string GetDingDan(string djbh)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data = new Dictionary<string, object>();
                    //使用convert（）截取时间段  23显示****-**-**  20
                    result = new DataBaseHelper().OnlineSql($"select ddx.djbh,chbm.mc,chbm.ywmc,chbm.rq,chbm.lb,chbm.gg,ddx.sl,CONVERT(varchar(100), ddx.hdrq, 23)as hdrq,ddx.xh from ddx join chbm on ddx.chbm=chbm.bm where ddx.djbh='{djbh}'order by djbh desc", true, 1);
                    if (result.State != ResultState.Success)
                        return result.ToString();
                    data.Add("ddmx", result.Object);

                    result.Object = data;
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);

        }


        /// <summary>
        /// 物料明细
        /// </summary>
        /// <param name="djbh"></param>
        /// <returns></returns>
        public string GetWuLiao(string djbh, string xh)
        {
            //定义返回集
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data = new Dictionary<string, object>();
                    //依据订单编号和订单序号查询订单详细界面的数据返回到显示界面
                    result = new DataBaseHelper().OnlineSql($"select chbm.mc,chbm.ywmc,cgx.ddbh,cgx.ddxh,cgx.djbh,cg.zdr,CONVERT(varchar(100), cg.rq, 23)as rq,chbm.gg from cg,cgx,chbm where cg.djbh=cgx.djbh and cgx.chbm=chbm.bm and cgx.ddbh='{djbh}'and cgx.ddxh='{xh}'", true, 1);
                    if (result.State != ResultState.Success)
                        return result.ToString();
                    //将数据加入到数据集中
                    data.Add("rwwl", result.Object);

                    //返回前端数据data中
                    result.Object = data;
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);


        }

        /// <summary>
        /// 生产明细
        /// </summary>
        /// <param name="djbh"></param>
        /// <returns></returns>
        public string GetShengChan(string djbh, string xh)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var data = new Dictionary<string, object>();
                    //利用子查询查明工序、编码对应表中中文
                    result = new DataBaseHelper().OnlineSql($"select chbm.mc,rwgx.cdjbh,rwgx.djbh,rwgx.cxh,rwgx.chbm,chbm.ywmc,rwgx.cxh,(select mc from bm where bm.lx='gx' and bm.bm=rwgx.gxbm) as gxbm,(select mc from bm where bm.lx='bm' and bm.bm=rwgx.bmbm) as bmbm,CONVERT(varchar(100), rwgx.kgrq, 23)as kgrq,CONVERT(varchar(100), rwgx.wgrq, 23)as wgrq from rwgx join chbm on rwgx.chbm=chbm.bm where rwgx.cdjbh='{djbh}'and rwgx.cxh='{xh}'and kgrq is not null", true, 1);
                    if (result.State != ResultState.Success)
                        return result.ToString();
                    data.Add("rwgx", result.Object);


                    result.Object = data;
                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", null).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                LogNet.WriteLog($"获取系统编码失败 ", ex);
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, $"{ex.Message}", null);
            }

            return JsonConvert.SerializeObject(result);


        }

    }
}

