﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace LibraryBasicApi.DataApi
{
    public class FileApiController : ControllerExc
    {
        /// <summary>
        /// 文件模板生成
        /// </summary>
        /// <param name="tempID">导入模板ID</param>
        /// <returns></returns>
        public string GetExcleTemps(string tempID)
        {
            var result = new Result();
            try
            {
                var list = JsonConvert.DeserializeObject<List<SysExcleImport>>(tempID);
                int count = 0;
                ExcleImprot ex = new ExcleImprot();
                foreach (var item in list)
                {
                    var rt = ex.GetExcleTemp(item.EimportId);
                    if (rt.State == "Success")
                    {
                        count++;
                    }
                }
                if (count == list.Count)
                {
                    result.State = ResultState.Success;
                    result.Message = "请求成功";
                }
                else if (count == 0)
                {
                    throw new Exception("数据生成失败！");
                }
                else
                {
                    result.State = ResultState.Warning;
                    result.Message = $"有{list.Count - count}条数据生成失败！";
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            return result.ToString();
        }

        /// <summary>
        /// 上传数据
        /// </summary>
        /// <param name="fileMid">已上传导入文件的ID</param>
        /// <returns></returns>
        public string ExcleFileImport(string fileMid)
        {
            var result = new Result();
            try
            {
                ExcleImprot ex = new ExcleImprot();
                return ex.ExcleImprotData(fileMid, LocalHost,true,Request).ToString();
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            return result.ToString();
        }

        /// <summary>
        /// 文件下载
        /// </summary>
        /// <returns></returns>
        public void GetManagerFile(string TempFile)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var query = db.Queryable<SysFileManager>().Where(x => x.ID == TempFile).Single();
                    var filePath = Server.MapPath(query.FilePath);
                    FileInfo info = new FileInfo(filePath);
                    long fileSize = info.Length;
                    HttpContext.Response.Clear();
                    HttpContext.Response.ContentType = "application/octet-stream";
                    //HttpContext.Response.AddHeader("Content-Disposition", "attachement;filename=" + Server.UrlEncode(info.FullName));
                    if (query.FileName.Contains("."))
                        HttpContext.Response.AddHeader("Content-Disposition", "attachement;filename=" + Server.UrlEncode(query.FileName));
                    else
                        HttpContext.Response.AddHeader("Content-Disposition", "attachement;filename=" + Server.UrlEncode(query.FileName+"."+query.FileType));
                    //指定文件大小   
                    HttpContext.Response.AddHeader("Content-Length", fileSize.ToString());
                    HttpContext.Response.WriteFile(filePath, 0, fileSize);
                    HttpContext.Response.Flush();
                    HttpContext.Response.Close();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
        }
    }
}
