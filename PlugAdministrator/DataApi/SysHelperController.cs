﻿using LibraryCore;
using LibraryCore.Core;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    [HandleErrorAttribute]
    public class SysHelperController : ControllerExc
    {
        /// <summary>
        /// 图标展示页
        /// 2019.09.05 - Casso
        /// </summary>
        /// <returns></returns>
        public ActionResult IconShow()
        {
            return View();
        }

        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string UploadFile(HttpPostedFileBase file)
        {
            var result = new Result();

            try
            {
                if (Request.Files.Count == 0)
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg004", ThisUser).FirstOrDefault()?.LanguageValue);
                }

                //计算此类文件放到那个文件夹
                string urlPath = string.Empty;
                string ex = Path.GetExtension(file.FileName);

                var config = SmartData.GetConfig("FilePath");
                var cfgList = JsonConvert.DeserializeObject<List<KeyValue>>(config.CfgValue);
                var cfg = cfgList.Find(x => !string.IsNullOrEmpty(x.Value) && x.Value.ToLower().Split(',').ToList().Contains(ex.Replace(".", "")));

                if (cfg != null)
                {
                    urlPath = cfg.Name;
                }
                else
                {
                    cfg = cfgList.Find(x => string.IsNullOrEmpty(x.Value));
                    if (cfg is null)
                    {
                        throw new Exception(new LanguageHelper().GetLanguage("msg008", ThisUser).FirstOrDefault()?.LanguageValue);
                    }
                    urlPath = string.IsNullOrEmpty(cfg.Name) ? "Resource" : cfg.Name;
                }
                if (cfg.ParentID != "-1")
                    urlPath = $"{urlPath}\\{DateTime.Now.ToString("yyyyMMddHH")}";

                string localPath = Path.Combine(HttpRuntime.AppDomainAppPath, urlPath);


                var filePathName = cfg.ParentID != "-1" ? Guid.NewGuid().ToString("N") + ex : file.FileName;
                if (!Directory.Exists(localPath))
                {
                    Directory.CreateDirectory(localPath);
                }
                file.SaveAs(Path.Combine(localPath, filePathName));

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", ThisUser).FirstOrDefault()?.LanguageValue;
                result.Object = $"\\{urlPath}\\{filePathName}";
            }
            catch (Exception ex)
            {
                LogNet.WriteLog($"文件上传方法 UploadFile", ex);
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }

            return result.ToString();

        }

        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string UploadFileManage(HttpPostedFileBase file)
        {
            var result = new Result();

            try
            {
                if (Request.Files.Count == 0)
                {
                    throw new Exception(new LanguageHelper().GetLanguage("msg004", ThisUser).FirstOrDefault()?.LanguageValue);
                }

                //计算此类文件放到那个文件夹
                string urlPath = string.Empty;
                string ex = Path.GetExtension(file.FileName);

                var config = SmartData.GetConfig("FilePath");
                var cfgList = JsonConvert.DeserializeObject<List<KeyValue>>(config.CfgValue);
                var cfg = cfgList.Find(x => !string.IsNullOrEmpty(x.Value) && x.Value.ToLower().Split(',').ToList().Contains(ex.Replace(".", "")));

                if (cfg != null)
                {
                    urlPath = cfg.Name;
                }
                else
                {
                    cfg = cfgList.Find(x => string.IsNullOrEmpty(x.Value));
                    if (cfg is null)
                    {
                        throw new Exception(new LanguageHelper().GetLanguage("msg008", ThisUser).FirstOrDefault()?.LanguageValue);
                    }
                    urlPath = string.IsNullOrEmpty(cfg.Name) ? "Resource" : cfg.Name;
                }
                if (cfg.ParentID != "-1")
                    urlPath = $"{urlPath}\\{DateTime.Now.ToString("yyyyMMddHH")}";

                string localPath = Path.Combine(HttpRuntime.AppDomainAppPath, urlPath);


                var filePathName = cfg.ParentID != "-1" ? Guid.NewGuid().ToString("N") + ex : file.FileName;
                if (!Directory.Exists(localPath))
                {
                    Directory.CreateDirectory(localPath);
                }
                file.SaveAs(Path.Combine(localPath, filePathName));

                using (var db = new DataBaseHelper().SugarClient())
                {
                    var sFile = new SysFileManager
                    {
                        ID = DataHelper.GenerateOrderNumber(),
                        CreateDate = DateTime.Now,
                        FileName = file.FileName,
                        FilePath = $"\\{urlPath}\\{filePathName}",
                        FileSize = file.ContentLength,
                        FileType = file.ContentType,
                        State = 1
                    };
                    db.Insertable(sFile).ExecuteCommand();

                    result.Object = sFile.ID;

                }

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", ThisUser).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception ex)
            {
                LogNet.WriteLog($"文件上传方法 UploadFile", ex);
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }

            return result.ToString();

        }

        #region 系统设置 

        /// <summary>
        /// 系统设置
        /// </summary>
        /// <returns></returns>
        public ActionResult SystemConfig()
        {
            return View();
        }

        /// <summary>
        /// 获取系统设置
        /// </summary>
        /// <param name="names"></param>
        /// <returns></returns>
        public string GetSystemSettingValue(string[] names)
        {
            var result = new Result();
            try
            {
                var map = new Dictionary<string, string>();
                foreach (var name in names)
                {
                    var value = ConfigHelper.ReadOtherConfigFile(name);
                    if (!map.ContainsKey(name))
                        map.Add(name, value);
                }
                result.State = ResultState.Success;
                result.Message = "获取成功";
                result.Object = map;
            }
            catch (Exception e)
            {
                result.State = ResultState.Error;
                result.Message = e.Message;
            }
            return result.ToString();
        }

        /// <summary>
        /// 系统设置修改
        /// </summary>
        /// <returns></returns>
        public string SystemSettingUpdate()
        {
            var result = new Result();
            try
            {
                var request = Request;

                foreach (var key in Request.Form.AllKeys)
                {
                    ConfigHelper.WriteOtherConfigFile(key, Request.Form[key]);
                }


                //如果Redis正在连接就重置一下
                RedisCache.CreateManager();

                result.State = ResultState.Success;
                result.Message = new LanguageHelper().GetLanguage("msg004", ThisUser).FirstOrDefault()?.LanguageValue;
            }
            catch (Exception e)
            {
                result.State = ResultState.Error;
                result.Message = e.Message;
            }
            return result.ToString();
        }

        #endregion

        #region 系统工具
        /// <summary>
        /// 系统工具
        /// </summary>
        /// <returns></returns>
        public ActionResult SystemTool()
        {
            return View();
        }

        /// <summary>
        /// 反向生成数据库
        /// </summary>
        /// <returns></returns>
        public string UpdataDataBaseAsModels()
        {

            var result = new DataBaseHelper().CreateDataBase();
            return result.ToString();
        }

        /// <summary>
        /// 在线执行SQL语句
        /// </summary>
        /// <returns></returns>
        public string OnlineExecSql()
        {
            var res = new DataBaseHelper().OnlineExecSql(Request["sql"]);
            return res.ToString();
        }

        /// <summary>
        /// 恢复初始化数据
        /// </summary>
        /// <returns></returns>
        public string RecoveryInitData()
        {
            var result = new Result();
            try
            {
                result.Message = new FileHelper().ReadFile($@"/Init/{ConfigHelper.ReadOtherConfigFile("DbType")}/{Request["tableName"]}.sql", System.Text.Encoding.UTF8);
                result.State = ResultState.Success;
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            return result.ToString();
        }

        public string ClearRedis()
        {

            var result = new Result();
            try
            {
                RedisCache.ClearRedis();
                result.State = ResultState.Success;
                result.Message = "缓存清除成功，数据再次使用时会重新加载数据可能导致加载缓慢问题。";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            return result.ToString();
        }

        #endregion

        #region 系统升级

        /// <summary>
        /// 系统升级
        /// </summary>
        /// <returns></returns>
        public ActionResult SystemInfo()
        {
            return View();
        }

        public string SysUploadRarView(HttpPostedFileBase file)
        {
            var resultStr = UploadFile(file);
            var res = JsonConvert.DeserializeObject<Result>(resultStr);
            if (res.State == ResultState.Success)
            {
                FileHelper.DeCompressRar(Path.Combine(HttpRuntime.AppDomainAppPath, res.Object.ToString().Substring(1)), Path.Combine(HttpRuntime.AppDomainAppPath, "Themes/"));
            }
            return res.ToString();
        }
        public string SysUploadRarService(HttpPostedFileBase file)
        {
            var resultStr = UploadFile(file);
            var res = JsonConvert.DeserializeObject<Result>(resultStr);
            if (res.State == ResultState.Success)
            {
                FileHelper.DeCompressRar(Path.Combine(HttpRuntime.AppDomainAppPath, res.Object.ToString().Substring(1)), Path.Combine(HttpRuntime.AppDomainAppPath, "Service/"));
            }
            return res.ToString();
        }
        #endregion

        /// <summary>
        /// 开发帮助
        /// </summary>
        /// <returns></returns>
        public ActionResult Development()
        {
            return View();
        }

        /// <summary>
        /// 可视化富文本
        /// </summary>
        /// <returns></returns>
        public ActionResult TextEditor()
        {
            return View();
        }

    }
}