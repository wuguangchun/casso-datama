﻿using LibraryCore;
using LibraryCore.Core;
using LibraryCore.Data;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;

namespace LibraryBasicApi.DataApi
{
    public class FlowDataApiController : ControllerExc
    {
        /// <summary>
        /// 获取视图的列数据
        /// </summary>
        /// <param name="viewID"></param>
        /// <returns></returns>
        public string GetAutoViewCol(string viewID)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var list = db.Queryable<SysViewColunms>()
                        .Where(x => x.ActionID == viewID)
                        .ToList();
                    var para = new List<KeyValue>();
                    list.OrderBy(x => x.Sort);
                    list.ForEach(x => para.Add(new KeyValue { Name = x.Title, Value = x.ViewColunmId, ParentID = x.Field }));
                    result.Object = para;
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    result.ToString(), null);
            }
            return result.ToString();

        }

        /// <summary>
        /// 存储审批流数据
        /// </summary>
        /// <returns></returns>
        public string SaveFlowData(string viewID, string approvalField, string name, string key, string desc, string flowJson)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var list = db.Queryable<SysFlow>().Where(x => x.ActionID == viewID).ToList();

                    var flow = new SysFlow
                    {
                        ID = list.Count < 1 ? DataHelper.GenerateOrderNumber() : list.First().ID,
                        ActionID = viewID,
                        ApprovalField = approvalField,
                        Name = name,
                        Key = key,
                        desc = desc,
                        Json = flowJson,
                        CreateDate = DateTime.Now,
                        CreateUser = ThisUser.Uid,

                    };

                    result = new FlowHelper().DataAnalysis(flow);
                    if (result.State != ResultState.Success)
                        return result.ToString();
                    db.Saveable(flow).ExecuteCommand();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 存储流程设计图
        /// </summary>
        /// <param name="flowid"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <param name="flowJson"></param>
        /// <returns></returns>
        public string SaveFlowChart(string flowid, string name, string desc, string flowJson)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var flow = db.Queryable<SysFlowChart>().Where(x => x.FlowId == flowid).ToList().FirstOrDefault();
                    if (flow == null)
                    {
                        flow = new SysFlowChart
                        {
                            FlowId = DataHelper.GenerateOrderNumber(),
                            Name = name,
                            desc = desc,
                            Json = flowJson,
                            updateDate = DateTime.Now,
                            CreateDate = DateTime.Now,
                            CreateUser = ThisUser.Uid,

                        };
                    }
                    else
                    {

                        //备份数据
                        flow.Name = name;
                        flow.desc = desc;
                        flow.JsonL6 = flow.JsonL5;
                        flow.JsonL5 = flow.JsonL4;
                        flow.JsonL4 = flow.JsonL3;
                        flow.JsonL3 = flow.JsonL2;
                        flow.JsonL2 = flow.JsonL1;
                        flow.JsonL1 = flow.Json;
                        flow.Json = flowJson;
                        flow.updateDate = DateTime.Now;
                    }

                    db.Saveable(flow).ExecuteCommand();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 获取流程图设计稿
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string GetFlowChart(string type)
        {

            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    if (string.IsNullOrEmpty(type))
                    {
                        var list = db.Queryable<SysFlowChart>()
                             .ToList();
                        list.ForEach(x => x.CreateUser = ThisUser.UserName);
                        result.Object = list;
                    }
                    else
                    {
                        var list = db.Queryable<SysFlowChart>()
                             .ToList();
                        list.ForEach(x => x.ReadUser.Contains(ThisUser.UserName));
                        result.Object = list;
                    }

                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, result.ToString(), null);
            }
            return result.ToString();

        }

        /// <summary>
        /// 获取审批数据项
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetViewAppr(string id)
        {
            return new FlowHelper().GetApproval(id).ToString();
        }

        /// <summary>
        /// 获取需要审批的详细数据
        /// </summary>
        /// <returns></returns>
        public string GetApprData(string id)
        {
            return new FlowHelper().GetApprovalData(id).ToString();
        }

        /// <summary>
        /// 获取等待审批列表
        /// </summary>
        /// <returns></returns>
        public string GetNeedAppr(string field, string order, string condition = null)
        {
            var result = new ResultDataTable();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var client = db.Queryable<SysFlowApproval>()
                        .Where(x => x.State == "review");

                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(order))
                    {
                        client.OrderBy($" {field} {order}");
                    }

                    if (!string.IsNullOrEmpty(condition))
                    {
                        var obj = JsonConvert.DeserializeObject<ConditionStr>(condition);
                        var conditions = JsonConvert.DeserializeObject<List<Condition>>(obj.jsonStr);
                        foreach (var condition1 in conditions)
                        {

                            switch (condition1.ConditionOptionVal)
                            {
                                case "equal":
                                    client.Where($"{condition1.ConditionFieldVal}= '{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "unequal":
                                    client.Where($"{condition1.ConditionFieldVal} <> '{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "like":
                                    client.Where($"{condition1.ConditionFieldVal} like '%{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "unlike":
                                    client.Where($"{condition1.ConditionFieldVal} not like '%{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "start":
                                    client.Where($"{condition1.ConditionFieldVal}  like '{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "end":
                                    client.Where($"{condition1.ConditionFieldVal}  like '%{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "empty":
                                    client.Where($"{condition1.ConditionFieldVal} is null");
                                    break;
                                case "between":
                                    client.Where($"{condition1.ConditionFieldVal} BETWEEN {condition1.ConditionValueLeftVal} and {condition1.ConditionValueRightVal}");
                                    break;
                            }
                        }
                    }

                    var list = client.ToList();

                    var detail = db.Queryable<SysFlowApprovalDetail>()
                        .Where(x => SqlFunc.ContainsArray(list.ConvertAll(z => z.ID), x.ApprovalID))
                        .ToList();
                    var listApprID = new List<string>();
                    foreach (var item in detail.FindAll(x => x.Executor.Contains(ThisUser.Uid) && string.IsNullOrEmpty(x.State)).ConvertAll(x => x.ApprovalID).Distinct())
                    {
                        var deltails = detail.Where(x => x.ApprovalID == item).OrderBy(x => x.sort).ToList();
                        if (!deltails.First().Executor.Contains(ThisUser.Uid))
                        {
                            var index = deltails.IndexOf(deltails.Find(x => x.Executor.Contains(ThisUser.Uid)));
                            var prev = deltails[index - 1];
                            if (string.IsNullOrEmpty(prev.State) || prev.State.ToLower() != "pass")
                            {
                                continue;
                            }
                        }


                        listApprID.Add(item);
                    }

                    var lastData = new List<SysFlowApproval>();
                    listApprID.ForEach(x => lastData.Add(list.Find(y => y.ID == x)));

                    result.data = lastData;
                    result.count = list.Count();
                    result.code = 0;

                }
            }
            catch (Exception ex)
            {
                result.code = -1;
                result.msg = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 获取审批过的列表
        /// </summary>
        /// <returns></returns>
        public string GetApprOver(int page = 1, int limit = 10, string field = null, string order = null, string condition = null)
        {
            var result = new ResultDataTable();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var client = db.Queryable<SysFlowApproval, SysFlowApprovalDetail>((appr, apprdetail) => appr.ID == apprdetail.ApprovalID)

                        .Where((appr, apprdetail) => !string.IsNullOrEmpty(appr.State) && (apprdetail.Reviewer == ThisUser.Uid || apprdetail.CCperson.Contains(ThisUser.Uid)));

                    if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(order))
                    {
                        client.OrderBy($" {field} {order}");
                    }

                    if (!string.IsNullOrEmpty(condition))
                    {
                        var obj = JsonConvert.DeserializeObject<ConditionStr>(condition);
                        var conditions = JsonConvert.DeserializeObject<List<Condition>>(obj.jsonStr);
                        foreach (var condition1 in conditions)
                        {

                            switch (condition1.ConditionOptionVal)
                            {
                                case "equal":
                                    client.Where($"{condition1.ConditionFieldVal}= '{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "unequal":
                                    client.Where($"{condition1.ConditionFieldVal} <> '{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "like":
                                    client.Where($"{condition1.ConditionFieldVal} like '%{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "unlike":
                                    client.Where($"{condition1.ConditionFieldVal} not like '%{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "start":
                                    client.Where($"{condition1.ConditionFieldVal}  like '{condition1.ConditionValueVal.Value}%'");
                                    break;
                                case "end":
                                    client.Where($"{condition1.ConditionFieldVal}  like '%{condition1.ConditionValueVal.Value}'");
                                    break;
                                case "empty":
                                    client.Where($"{condition1.ConditionFieldVal} is null");
                                    break;
                                case "between":
                                    client.Where($"{condition1.ConditionFieldVal} BETWEEN {condition1.ConditionValueLeftVal} and {condition1.ConditionValueRightVal}");
                                    break;
                            }
                        }
                    }
                    var list = client.Select<SysFlowApproval>().ToList().ConvertAll(x => x.ID).Distinct().ToList();

                    int count = 0;
                    result.data = db.Queryable<SysFlowApproval>()
                        .Where(x => SqlFunc.ContainsArray(list, x.ID))
                        .ToPageList(page, limit, ref count);


                    result.count = count;
                    result.code = 0;

                }
            }
            catch (Exception ex)
            {
                result.code = -1;
                result.msg = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 用户执行审批
        /// </summary>
        /// <param name="apprID"></param>
        /// <param name="state"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public string ExecAppr(string apprID, string state, string remark)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var appr = db.Queryable<SysFlowApproval>().Where(x => x.ID == apprID).First();
                    var apprDetail = db.Queryable<SysFlowApprovalDetail>().Where(x => x.ApprovalID == apprID).OrderBy(x => x.sort).ToList();

                    var need = apprDetail.Find(x => string.IsNullOrEmpty(x.State) && x.Executor.Contains(ThisUser.Uid));
                    if (appr == null || need == null)
                    {
                        throw new Exception("未找到与你有关的审批项！");
                    }

                    var index = apprDetail.IndexOf(need);
                    var prev = index + 1 < apprDetail.Count ? apprDetail[index + 1] : null;

                    need.State = state;
                    need.Reviewer = ThisUser.Uid;
                    need.Remarks = remark;
                    need.ApprovalDate = DateTime.Now.ToString();
                    if (db.Updateable(need).ExecuteCommand() < 1)
                    {
                        throw new Exception("服务器异常了，请稍微等待后再次提交！");
                    }

                    if (prev == null || state.ToLower() != "pass")
                    {
                        appr.State = state;
                        if (db.Updateable(appr).ExecuteCommand() < 1)
                        {
                            throw new Exception("服务器异常了，请稍微等待后再次提交！");
                        }

                        if (prev == null && state.ToLower() == "pass")
                        {
                            //修改数据 
                            try
                            {
                                var action = db.Queryable<SysAutoView>().Where(x => x.ActionID == appr.ActionID).Single();
                                SmartAutoView.SmartUri(action);
                                var reJson = string.Empty;
                                string encrypkey = ConfigurationSettings.AppSettings["Encrypkey"];
                                EncryptHelper encrypt = new EncryptHelper(encrypkey.Substring(0, 24));
                                var sponsor = db.Queryable<SysUserInfo>().Where(x => x.Uid == appr.Sponsor).ToList().FirstOrDefault();
                                var sponsorJson = JsonConvert.SerializeObject(sponsor == null ? ThisUser : sponsor);
                                new PushWebHelper().PushFormToPost($"{Request.Url.Scheme}://localhost:{Request.Url.Port}" + action.EditUrl, appr.DataSet, ref reJson, new List<System.Net.Cookie> { new System.Net.Cookie { Name = "WebUser", Value = encrypt.Encrypt(sponsorJson), Domain = "localhost" } });
                                result = JsonConvert.DeserializeObject<Result>(reJson);
                                if (result.State != ResultState.Success)
                                {
                                    throw new Exception(result.Message);
                                }
                            }
                            catch (Exception ex)
                            {
                                need.State = null;
                                need.Reviewer = null;
                                need.Remarks = null;
                                need.ApprovalDate = null;
                                if (db.Updateable(need).ExecuteCommand() < 1)
                                {
                                    throw new Exception("服务器异常了，请稍微等待后再次提交！");
                                }

                                appr.State = "review";
                                if (db.Updateable(appr).ExecuteCommand() < 1)
                                {
                                    throw new Exception("服务器异常了，请稍微等待后再次提交！");
                                }
                                throw ex;
                            }
                        }

                    }

                    if (!string.IsNullOrEmpty(need.CCperson))
                    { 
                        MessageHelper.InsertMessage(new Result("您有一条新审批抄送单据！") { Object = need }, MessageType.Whole, need.CCperson);
                    }
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 获取流程发起列表
        /// </summary>
        /// <returns></returns>
        public string GetFlowAppr()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    result.Object = db.Queryable<SysAutoView>().Where(x => x.Approval == "Y").ToList();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 获取审批项的表单信息
        /// </summary>
        /// <returns></returns>
        public string GetFolwApprFormItem(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var colList = db.Queryable<SysViewColunms>().Where(x => x.ActionID == id).ToList();

                    //参数获取
                    var temps = colList.ConvertAll(x => x.Templet).Distinct();
                    Dictionary<string, List<KeyValue>> paras = new Dictionary<string, List<KeyValue>>();
                    foreach (var item in temps)
                    {
                        paras.Add(item, DataBaseHelper.GetTempPara(item, ThisUser));
                    }
                    var temp = db.Queryable<SysParameter>().Where(x => SqlFunc.ContainsArray(temps.ToArray(), x.TempID)).ToList();
                    var formtemp = new FormTemp("layui-col-md4");
                    //生成表单HTML
                    foreach (var item in colList.OrderBy(x => x.Sorting))
                    {
                        if (item.ReadOnly == 3 || item.ReadOnly == 4)
                        {
                            continue;
                        }
                        var para = new List<KeyValue>();
                        if (!string.IsNullOrEmpty(item.Templet) && temp.Find(x => x.TempID == item.Templet).TempType.ToLower() == "temp")
                        {
                            para = paras[item.Templet];
                            item.Templet = null;
                        }
                        else if (item.EditType.ToLower().Contains("selectxm"))
                        {
                            para = paras[item.Templet];
                            item.Templet = null;
                        }

                        switch (item.EditType.ToLower())
                        {
                            case "password":
                                result.Object += formtemp.PassWord(item.Title, item.Field, item.ViewColunmId, item.DefaultVal, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "date":
                                result.Object += formtemp.DateTime(item.Title, item.Field, item.ViewColunmId, item.DefaultVal, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "select":
                                result.Object += formtemp.Select(item.Title, item.Field, item.Templet, para, item.ViewColunmId, false, item.Required);
                                break;
                            case "selectinput":
                                result.Object += formtemp.Select(item.Title, item.Field, item.Templet, para, item.ViewColunmId, false, item.Required);
                                break;
                            case "selectserach":
                                result.Object += formtemp.Select(item.Title, item.Field, item.Templet, para, item.ViewColunmId, true, item.Required);
                                break;
                            case "selecttree":
                                result.Object += formtemp.Select(item.Title, item.Field, item.Templet, para, item.ViewColunmId, true, item.Required);
                                break;
                            case "selectxm":
                                result.Object += formtemp.SelectMulti(item.Title, item.Field, item.Templet, para, item.ViewColunmId, true, item.Required);
                                break;
                            case "selectxmtree":
                                result.Object += formtemp.SelectMulti(item.Title, item.Field, item.Templet, para, item.ViewColunmId, true, item.Required);
                                break;
                            case "upload-audio":
                                result.Object += formtemp.File(item.Title, item.Field, item.Field, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "upload-video":
                                result.Object += formtemp.File(item.Title, item.Field, item.Field, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "upload-images":
                                result.Object += formtemp.File(item.Title, item.Field, item.Field, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "upload-file":
                                result.Object += formtemp.File(item.Title, item.Field, item.Field, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "texteditor":
                                result.Object += formtemp.Textarea(item.Title, item.Field, item.ViewColunmId, item.DefaultVal, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "checkboxgroup":
                                result.Object += formtemp.Checkbox(item.Title, item.Field, paras[item.Templet], item.ViewColunmId);
                                break;
                            case "checkbox":
                                result.Object += formtemp.Checkbox(item.Title, item.Field, paras[item.Templet], item.ViewColunmId);
                                break;
                            case "number":
                                result.Object += formtemp.Number(item.Title, item.Field, item.ViewColunmId, item.DefaultVal, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            case "textarea":
                                result.Object += formtemp.Textarea(item.Title, item.Field, item.ViewColunmId, item.DefaultVal, item.DefaultVal, item.Required, item.ReadOnly != 2);
                                break;
                            default:
                                result.Object += formtemp.Input(item.Title, item.Field, item.ViewColunmId, item.DefaultVal, item.DefaultVal, item.Required, item.ReadOnly != 2); ;
                                break;
                        }


                    }
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 发起审批申请-数据暂存模式
        /// </summary>
        /// <param name="apprID"></param>
        /// <returns></returns>
        public string ApprCreate(string ApprCreate)
        {
            return new FlowHelper().ApprCreate(ApprCreate, Request, ThisUser).ToString();
        }

        /// <summary>
        /// 获取审批节点列表
        /// </summary>
        /// <returns></returns>
        public string GetApprNodes(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {

                    var apprDetail = db.Queryable<SysFlowApprovalDetail>().Where(x => x.ApprovalID == id).OrderBy(x=>x.sort).ToList();


                    var userList = db.Queryable<SysUserInfo>().ToList();
                    foreach (var item in apprDetail)
                    {
                        var exeUser = new List<SysUserInfo>();
                        if (!string.IsNullOrEmpty(item.Reviewer))
                        {
                            item.Reviewer.Split(',').ToList().ForEach(x => exeUser.Add(userList.Find(u => u.Uid == x)));
                            exeUser.RemoveAll(x => x == null);
                            item.Reviewer = string.Join(",", exeUser.ConvertAll(x => x.UserName));
                        }

                        if (!string.IsNullOrEmpty(item.Executor))
                        {
                            exeUser = new List<SysUserInfo>();
                            item.Executor.Split(',').ToList().ForEach(x => exeUser.Add(userList.Find(u => u.Uid == x)));
                            exeUser.RemoveAll(x => x == null);
                            item.Executor = string.Join(",", exeUser.ConvertAll(x => x.UserName));

                        }
                        if (!string.IsNullOrEmpty(item.CCperson))
                        {
                            exeUser = new List<SysUserInfo>();
                            item.CCperson.Split(',').ToList().ForEach(x => exeUser.Add(userList.Find(u => u.Uid == x)));
                            exeUser.RemoveAll(x => x == null);
                            item.CCperson = string.Join(",", exeUser.ConvertAll(x => x.UserName));

                        }

                    }

                    result.Object = apprDetail;
                }
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

    }
}