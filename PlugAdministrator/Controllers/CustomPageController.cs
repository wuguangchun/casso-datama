﻿using LibraryCore;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LibraryAdministrator.Controllers
{
    public class CustomPageController : ControllerExc
    {
        public ActionResult CustomJurisdiction()
        {
            return View();
        }
    }
}
