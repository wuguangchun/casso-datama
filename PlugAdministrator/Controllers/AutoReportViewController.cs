﻿using LibraryCore.ActionFilter;
using LibraryCore.AutoView;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    [ExceptionAttribute]
    public class AutoReportViewController : ControllerReport
    {
        /// <summary>
        /// 自动表格智慧页
        /// 2019.08.12 - Casso
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            AutoView();
            return View();
        }


    }
}