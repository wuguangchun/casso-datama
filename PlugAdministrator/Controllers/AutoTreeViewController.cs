﻿using LibraryCore;
using LibraryCore.ActionFilter;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    [ExceptionAttribute]
    public class AutoTreeViewController : ControllerTree
    {
        /// <summary>
        /// 自动树形智慧页
        /// 2019.08.12 - Casso
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            AutoView();
            return View();
        }

    }
}