﻿using LibraryAdministrator.ActionFilter;
using LibraryCore;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    [LoginActionFilter]
    public class AdministratorController : ControllerExc
    {
        /// <summary>
        /// 后台首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.User = ThisUser;
            InitSysLanguage();
            return View();
        }

        /// <summary>
        /// 登陆界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            InitSysLanguage();
            return View();
        }
    }
}