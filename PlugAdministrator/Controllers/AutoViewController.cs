﻿using LibraryCore;
using LibraryCore.ActionFilter;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    [ExceptionAttribute]
    public class AutoViewController : ControllerTable
    {
        /// <summary>
        /// 自动表格智慧页
        /// 2019.08.12 - Casso
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string bar="s")
        {
            AutoView();
            return bar=="s"?View():View("IndexNoBar");
        }

        /// <summary>
        /// 自动多表格智慧页
        /// 2020.08.11 - Casso
        /// </summary>
        /// <returns></returns>
        public ActionResult MultipleTables()
        {
            AutoView();
            return View();
        }
    }
}