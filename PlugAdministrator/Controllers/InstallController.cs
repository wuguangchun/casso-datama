﻿using LibraryAdministrator.ActionFilter;
using LibraryCore;
using LibraryCore.Core;
using LibraryCore.Data;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TengYunCore;

namespace RazorView.Controllers
{
    public class InstallController : Controller
    {

        /// <summary>
        /// 程序安装
        /// </summary>
        /// <returns></returns>
        public ActionResult InstallApplication()
        {
            return View();
        }
    }
}