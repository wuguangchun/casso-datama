﻿using LibraryCore;
using LibraryCore.Data;
using LibraryModel.OtherModels;
using System;
using System.Reflection;
using System.Web.Http;

namespace LibraryBasicApi.WebApi
{
    /// <summary>
    /// 系统通用接口
    /// </summary>
    public class SystemController : ApiController
    {
        /// <summary>
        /// 根据用户名密码获取用户令牌
        /// </summary>
        /// <param name="uName">用户名</param>
        /// <param name="Upwd">用户密码</param>
        /// <returns></returns>
        [HttpGet]
        public Result GetUserToken(string uName, string Upwd)
        {
            var result = new Result();
            try
            {
                result = new ApiHelper().NewToken(uName, Upwd);
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
            }
            finally
            {
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name, result.ToString(), null);
            }
            return result;
        }

        [HttpPost]
        public Result Getjson([FromBody] Result result)
        {
            return result;
        }
    }
}
