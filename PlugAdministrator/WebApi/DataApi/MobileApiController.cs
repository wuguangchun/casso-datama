﻿using LibraryCore;
using LibraryCore.Data;
using LibraryCore.WorkWeixin;
using LibraryModel.DataModel;
using LibraryModel.OtherModels;
using Newtonsoft.Json;
using ServiceStack;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;

namespace LibraryBasicApi.Controllers
{
    public class MobileApiController : ControllerExc
    {
        #region 付款申请单
        /// <summary>
        /// 付款单申请
        /// </summary>
        /// <returns></returns>
        public string GetTaskFq()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    var mk = db.Queryable<xmk>()
                        .Where(x => x.bm == "170084")
                        .Single();

                    var qx = db.Queryable<xqx>()
                        .Where(x => x.mkbm == "170084" && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                        .Single();

                    var list = new List<fq>();
                    if (qx != null)
                    {
                        list = db.Queryable<fq>()
                           .Where(x => x.shbz != "1")
                           .ToList();

                        switch (qx.sh)
                        {
                            case "1":
                                list.RemoveAll(x => x.sh1 == "1");
                                break;
                            case "2":
                                list.RemoveAll(x => x.sh2 == "1" || x.sh1 != "1");
                                break;
                            case "3":
                                list.RemoveAll(x => x.sh3 == "1" || x.sh2 != "1");
                                break;
                            case "4":
                                list.RemoveAll(x => x.sh4 == "1" || x.sh3 != "1");
                                break;
                            case "5":
                                list.RemoveAll(x => x.sh5 == "1" || x.sh4 != "1");
                                break;
                            case "6":
                                list.RemoveAll(x => x.sh6 == "1" || x.sh5 != "1");
                                break;
                            case "7":
                                list.RemoveAll(x => x.sh7 == "1" || x.sh6 != "1");
                                break;
                            case "8":
                                list.RemoveAll(x => x.sh8 == "1" || x.sh7 != "1");
                                break;
                            case "9":
                                list.RemoveAll(x => x.sh9 == "1" || x.sh8 != "1");
                                break;
                            default:
                                list.RemoveAll(x => true);
                                break;

                        }
                    }

                    result.Object = new List<object> { list ?? new List<fq>(), new Approval { Approvals = true, Detailed = true, JoinInvitation = false, JointTrial = false } };
                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// 付款单申请
        /// </summary>
        /// <returns></returns>ds
        public string GetTaskvFq(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    var fq = db.Queryable<vfq>()
                        .Where(x => x.djbh == id)
                        .Single();

                    fq.bz = string.IsNullOrEmpty(fq.bz) ? "无" : fq.bz;
                    fq.csmc = string.IsNullOrEmpty(fq.csmc) ? "无" : fq.csmc;

                    var qx = db.Queryable<xqx>()
                        .Where(x => x.mkbm == "170084" && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                        .Single();

                    result.Object = fq;
                    switch (qx.sh)
                    {
                        case "1":
                            if (fq.sh1 == "1")
                                throw new Exception("该节点已审批，如需查看请移步至电脑端！");
                            break;
                        case "2":
                            if (fq.sh2 == "1")
                                throw new Exception("该节点已审批，如需查看请移步至电脑端！");
                            if (fq.sh1 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "3":
                            if (fq.sh3 == "1")
                                throw new Exception("该节点已审批，如需查看请移步至电脑端！");
                            if (fq.sh2 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "4":
                            if (fq.sh4 == "1")
                                throw new Exception("该节点已审批，如需查看请移步至电脑端！");
                            if (fq.sh3 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "5":
                            if (fq.sh5 == "1")
                                throw new Exception("该节点已审批，如需查看请移步至电脑端！");
                            if (fq.sh4 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "6":
                            if (fq.sh6 == "1")
                                throw new Exception("该节点已审批，如需查看请移步至电脑端！");
                            if (fq.sh5 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;

                    }


                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 付款单明细
        /// </summary>
        /// <returns></returns>
        public string GetTaskFqx(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    var list = db.Queryable<fqx>()
                        .Where(x => x.djbh == id)
                        .ToList();

                    result.Object = list;

                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 付款单审核通过
        /// </summary>
        /// <returns></returns>
        public string TaskFqExamine(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    //模块表内涵最大审核级次
                    var mk = db.Queryable<xmk>()
                       .Where(x => x.bm == "170084")
                       .Single();

                    //系统权限表   内含当前人审批级次
                    var qx = db.Queryable<xqx>()
                       .Where(x => x.mkbm == "170084" && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                       .Single();

                    if (qx != null)
                    {

                        var fqd = db.Queryable<fq>()
                            .Where(x => x.djbh == id)
                            .Single();

                        switch (qx.sh)
                        {
                            case "1":
                                fqd.sh1 = "1";
                                fqd.sr1 = ThisUser.UserName;
                                fqd.sq1 = DateTime.Now;
                                break;
                            case "2":
                                fqd.sh2 = "1";
                                fqd.sr2 = ThisUser.UserName;
                                fqd.sq2 = DateTime.Now;
                                break;
                            case "3":
                                fqd.sh3 = "1";
                                fqd.sr3 = ThisUser.UserName;
                                fqd.sq3 = DateTime.Now;
                                break;
                            case "4":
                                fqd.sh4 = "1";
                                fqd.sr4 = ThisUser.UserName;
                                fqd.sq4 = DateTime.Now;
                                break;
                            case "5":
                                fqd.sh5 = "1";
                                fqd.sr5 = ThisUser.UserName;
                                fqd.sq5 = DateTime.Now;
                                break;
                            case "6":
                                fqd.sh6 = "1";
                                fqd.sr6 = ThisUser.UserName;
                                fqd.sq6 = DateTime.Now;
                                break;
                            case "7":
                                fqd.sh7 = "1";
                                fqd.sr7 = ThisUser.UserName;
                                fqd.sq7 = DateTime.Now;
                                break;
                            case "8":
                                fqd.sh8 = "1";
                                fqd.sr8 = ThisUser.UserName;
                                fqd.sq8 = DateTime.Now;
                                break;
                            case "9":
                                fqd.sh9 = "1";
                                fqd.sr9 = ThisUser.UserName;
                                fqd.sq9 = DateTime.Now;
                                break;

                        }

                        if (mk.shjs == qx.sh)
                        {
                            fqd.shbz = "1";
                        }

                        db.Saveable(fqd).ExecuteCommand();
                    }
                    else
                    {
                        throw new Exception("抱歉，您并没有审核权限！");
                    }

                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        #endregion

        #region 不合格评审单

        public string GetTaskQc(string mkbm)
        {
            var result = new Result();
            try
            {

                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    var mk = db.Queryable<xmk>()
                        .Where(x => x.bm == mkbm)
                        .Single();

                    var qx = db.Queryable<xqx>()
                        .Where(x => x.mkbm == mkbm && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                        .Single();
                    var list = new List<zl>();


                    if (qx != null)
                    {
                        list = db.Queryable<zl>()
                           .Where(x => x.shbz != "1" && x.djbh.StartsWith($"{mk.djlx}%") && x.hszt == "1")
                           .ToList().FindAll(x => string.IsNullOrEmpty(x.mkbm));

                        switch (qx.sh)
                        {
                            case "1":
                                list.RemoveAll(x => x.sh1 == "1");
                                break;
                            case "2":
                                list.RemoveAll(x => x.sh2 == "1" || x.sh1 != "1");
                                break;
                            case "3":
                                list.RemoveAll(x => x.sh3 == "1" || x.sh2 != "1");
                                break;
                            case "4":
                                list.RemoveAll(x => x.sh4 == "1" || x.sh3 != "1");
                                break;
                            case "5":
                                list.RemoveAll(x => x.sh5 == "1" || x.sh4 != "1");
                                break;
                            case "6":
                                list.RemoveAll(x => x.sh6 == "1" || x.sh5 != "1");
                                break;
                            default:
                                list.RemoveAll(x => true);
                                break;

                        }

                    }

                    var tlist = db.Queryable<zl>()
                           .Where(x => x.shbz != "1" && x.djbh.StartsWith($"{mk.djlx}%") && x.hszt == "1")
                           .ToList();
                    tlist.RemoveAll(x => string.IsNullOrEmpty(x.mkbm));

                    foreach (var item in tlist)
                    {
                        qx = db.Queryable<xqx>()
                        .Where(x => x.mkbm == item.mkbm && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                        .Single();


                        if (qx != null)
                        {

                            switch (qx.sh)
                            {
                                case "1":
                                    if (item.sh1 == "1")
                                        continue;
                                    break;
                                case "2":
                                    if (item.sh2 == "1" || item.sh1 != "1")
                                        continue;
                                    break;
                                case "3":
                                    if (item.sh3 == "1" || item.sh2 != "1")
                                        continue;
                                    break;
                                case "4":
                                    if (item.sh4 == "1" || item.sh3 != "1")
                                        continue;
                                    break;
                                case "5":
                                    if (item.sh5 == "1" || item.sh4 != "1")
                                        continue;
                                    break;
                                case "6":
                                    if (item.sh6 == "1" || item.sh5 != "1")
                                        continue;
                                    break;

                            }

                            list.Add(item);
                        }
                    }

                    var approval = new Approval { Approvals = true, Detailed = false, JoinInvitation = false, JointTrial = true };

                    result.Object = new List<object> { list ?? new List<zl>(), approval };
                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }


        /// <summary>
        /// 不合格评审单
        /// </summary>
        /// <returns></returns>ds
        public string GetTaskVqc(string id, string mkbm)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    var spds = db.Queryable<vzl>()
                        .Where(x => x.djbh == id)
                        .ToList();

                    var spdmk = db.Queryable<zl>()
                      .Where(x => x.djbh == id)
                      .Single();

                    var qx = db.Queryable<xqx>()
                        .Where(x => x.mkbm == (string.IsNullOrEmpty(spdmk.mkbm) ? mkbm : spdmk.mkbm) && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                        .Single();
                    var spd = spds.First();
                    spds.ForEach(x => x.sr1 = string.IsNullOrEmpty(x.sr1) ? "无" : x.sr1);
                    spds.ForEach(x => x.sr2 = string.IsNullOrEmpty(x.sr2) ? "无" : x.sr2);
                    spds.ForEach(x => x.sr3 = string.IsNullOrEmpty(x.sr3) ? "无" : x.sr3);
                    spds.ForEach(x => x.sr4 = string.IsNullOrEmpty(x.sr4) ? "无" : x.sr4);

                    result.Object = spds;
                    result.State = ResultState.Success;
                    result.Message = "请求成功";

                    switch (qx?.sh)
                    {
                        case "1":
                            if (spd.sh1 == "1")
                            {
                                result.State = ResultState.Warning;
                            }
                            break;
                        case "2":
                            if (spd.sh2 == "1")
                            {
                                result.State = ResultState.Warning;
                            }
                            if (spd.sh1 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "3":
                            if (spd.sh3 == "1")
                            {
                                result.State = ResultState.Warning;
                            }
                            if (spd.sh2 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "4":
                            if (spd.sh4 == "1")
                            {
                                result.State = ResultState.Warning;
                            }
                            if (spd.sh3 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "5":
                            if (spd.sh5 == "1")
                            {
                                result.State = ResultState.Warning;
                            }
                            if (spd.sh4 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;
                        case "6":
                            if (spd.sh6 == "1")
                            {
                                result.State = ResultState.Warning;
                            }
                            if (spd.sh5 != "1")
                                throw new Exception("亲，您有点操之过急哦，上一节点还未审批完成！");
                            break;

                    }


                }

            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 会审明细
        /// </summary>
        /// <returns></returns>ds
        public string GetTaskHsmx(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {

                    var hsList = db.Queryable<hsmx>().Where(x => x.djbh == id).ToList();


                    result.Object = hsList;
                    result.State = hsList.Find(x => x.psr == ThisUser.UserName) != null ? ResultState.Success : ResultState.Warning;
                    result.Message = "请求成功";

                }

            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 意见收集
        /// </summary>
        /// <returns></returns>
        public string OpinionCollection(string id, string opinion)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {

                    var psd = db.Queryable<zl>().Where(x => x.djbh == id).Single();
                    if (psd == null && psd.hszt == "1")
                    {
                        throw new Exception("会审结果已经收集完毕，或者会审单据已关闭！");
                    }

                    var hsmx = db.Queryable<hsmx>().Where(x => x.djbh == id && x.psr == ThisUser.UserName).Single();
                    if (hsmx != null)
                    {
                        if (!string.IsNullOrEmpty(hsmx.psyj))
                        {
                            throw new Exception("您已经提交过意见了!");
                        }

                        hsmx.psyj += $"{opinion}";
                        hsmx.psrq = DateTime.Now;
                        db.Saveable(hsmx).ExecuteCommand();

                        var hsmxs = db.Queryable<hsmx>().Where(x => x.djbh == id && (x.psyj == null || x.psyj == "")).ToList();
                        if (hsmxs == null || hsmxs.Count < 1)
                        {
                            psd.hszt = "1";
                            db.Saveable(psd).ExecuteCommand();
                        }
                    }
                    else
                    {
                        throw new Exception("哎吆吆，没有找到需要您提交的会审意见呐！");
                    }
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }


        /// <summary>
        /// 品质评审单审核通过
        /// </summary>
        /// <returns></returns>
        public string TaskQcExamine(string id, string mkbm)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    //拿到当前审批单据
                    var spd = db.Queryable<zl>()
                        .Where(x => x.djbh == id)
                        .Single();

                    //模块表内涵最大审核级次
                    var mk = db.Queryable<xmk>()
                       .Where(x => x.bm == (string.IsNullOrEmpty(spd.mkbm) ? mkbm : spd.mkbm))
                       .Single();


                    //系统权限表   内含当前人审批级次
                    var qx = db.Queryable<xqx>()
                       .Where(x => x.mkbm == (string.IsNullOrEmpty(spd.mkbm) ? mkbm : spd.mkbm) && x.usrbm == ThisUser.UserName && x.sh != "0" && x.sh != "" && x.sh != null)
                       .Single();

                    if (qx != null)
                    {


                        switch (qx.sh)
                        {
                            case "1":
                                spd.sh1 = "1";
                                spd.sr1 = ThisUser.UserName;
                                spd.sq1 = DateTime.Now;
                                break;
                            case "2":
                                spd.sh2 = "1";
                                spd.sr2 = ThisUser.UserName;
                                spd.sq2 = DateTime.Now;
                                break;
                            case "3":
                                spd.sh3 = "1";
                                spd.sr3 = ThisUser.UserName;
                                spd.sq3 = DateTime.Now;
                                break;
                            case "4":
                                spd.sh4 = "1";
                                spd.sr4 = ThisUser.UserName;
                                spd.sq4 = DateTime.Now;
                                break;
                            case "5":
                                spd.sh5 = "1";
                                spd.sr5 = ThisUser.UserName;
                                spd.sq5 = DateTime.Now;
                                break;
                            case "6":
                                spd.sh6 = "1";
                                spd.sr6 = ThisUser.UserName;
                                spd.sq6 = DateTime.Now;
                                break;
                            case "7":
                                spd.sh7 = "1";
                                spd.sr7 = ThisUser.UserName;
                                spd.sq7 = DateTime.Now;
                                break;
                            case "8":
                                spd.sh8 = "1";
                                spd.sr8 = ThisUser.UserName;
                                spd.sq8 = DateTime.Now;
                                break;
                            case "9":
                                spd.sh9 = "1";
                                spd.sr9 = ThisUser.UserName;
                                spd.sq9 = DateTime.Now;
                                break;

                        }

                        if (mk.shjs == qx.sh)
                        {
                            spd.shbz = "1";
                        }

                        db.Saveable(spd).ExecuteCommand();
                    }
                    else
                    {
                        throw new Exception("抱歉，您并没有审核权限！");
                    }

                }

                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        #endregion

        #region 会审单

        public string GetTaskJointTrial(string mkbm)
        {
            var result = new Result();
            try

            {
                using (var db = new DataBaseHelper().SugarClient(1))
                {
                    var mk = db.Queryable<xmk>()
                         .Where(x => x.bm == mkbm)
                         .Single();

                    var qx = db.Queryable<xqx>()
                        .Where(x => x.mkbm == mkbm && x.usrbm == ThisUser.UserName && x.hs == "1")
                        .Single();

                    var psd = db.Queryable<zl>().Where(x => x.shbz != "1" && x.hszt != "1" && x.djbh.StartsWith($"{mk.djlx}%")).ToList();

                    var list = new List<hsmx>();

                    foreach (var item in psd)
                    {
                        var obj = db.Queryable<hsmx>().Where(y => y.djbh == item.djbh && y.psr == ThisUser.UserName && SqlFunc.IsNullOrEmpty(y.psyj)).ToList();

                        list.AddRange(obj);
                    }

                    var listpsd = new List<zl>();
                    foreach (var item in list.ConvertAll(x => x.djbh).Distinct())
                    {
                        var dj = psd.Find(x => x.djbh == item);
                        if (dj != null)
                        {
                            listpsd.Add(dj);
                        }
                    }
                    var approval = new Approval { Approvals = false, Detailed = false, JoinInvitation = qx?.hs == "1", JointTrial = true };
                    result.Object = new List<object> { listpsd, approval };
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        #endregion

        #region 系统公告

        /// <summary>
        /// 系统公告列表获取
        /// </summary>
        /// <returns></returns>
        public string GetNotice()
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var org = ThisUser.OrgAttrId.Split(',');
                    var list = new List<SysNotice>();
                    foreach (var item in org)
                    {
                        var rList = db.Queryable<SysNotice>()
                            .Where(x => x.SendUserOrg.Contains(item))
                            .ToList();

                        foreach (var notice in rList)
                        {
                            if (list.Find(x => x.NoticeId == notice.NoticeId) == null)
                            {
                                list.Add(notice);
                            }
                        }
                    }

                    result.Object = list;

                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 公告详细内容获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetNoticeDetail(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    result.Object = db.Queryable<SysNotice>()
                        .Where(x => x.NoticeId == id)
                        .Single();
                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public string SendNotice(string id)
        {
            var result = new Result();
            try
            {
                using (var db = new DataBaseHelper().SugarClient())
                {
                    var list = JsonConvert.DeserializeObject<List<SysNotice>>(id);
                    var notice = db.Queryable<SysNotice>()
                         .Where(x => x.NoticeId == list.First().NoticeId)
                         .Single();

                    foreach (var sOrg in notice.SendUserOrg.Split(','))
                    {
                        var users = db.Queryable<SysUserInfo>()
                             .Where(x => x.OrgAttrId.Contains(sOrg))
                             .ToList();

                        foreach (var item in users)
                        {
                            new WorkWechatHelper().SendMessageToUser(item.WeUserId, "text",
                                   $"{item.UserName} 您好，收到一条新公告，{notice.Title},\n 介绍：{notice.Description}\n 通知时间：{DateTime.Now.ToString("yyyy-MM-dd hh:mm")}\n <a href='{Request.Url.Scheme}://{Request.Url.Host}:{Request.Url.Port}/MobileAdmin/NoticeDetail?id={notice.NoticeId}'>点击查看</a>");
                        }
                    }

                }
                result.State = ResultState.Success;
                result.Message = "请求成功";
            }
            catch (Exception ex)
            {
                result.State = ResultState.Error;
                result.Message = ex.Message;
                new LogNetDataBase().SysImpleLog(MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name,
                    ex.Message, null);
            }
            return JsonConvert.SerializeObject(result);
        }

        #endregion


    }
}
