﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    public class SystemToolController : Controller
    {
        /// <summary>
        /// Excle 数据导入文件上传页面
        /// </summary>
        /// <returns></returns>
        public ActionResult ExcleImport()
        {
            return View();
        }

        /// <summary>
        /// 系统日志打印页面
        /// </summary>
        /// <returns></returns>
        public ActionResult LogMqPrint()
        {
            return View();
        }

        /// <summary>
        /// 工作流程设计
        /// </summary>
        /// <returns></returns>
        public ActionResult WorkFlow()
        {
            return View();
        }

        /// <summary>
        /// 流程图设计
        /// </summary>
        /// <returns></returns>
        public ActionResult FlowChart()
        {
            return View();
        }

        /// <summary>
        /// 待我审批
        /// </summary>
        /// <returns></returns>
        public ActionResult MyFlowApproval()
        {
            return View();
        }

        /// <summary>
        /// 审批详情展示
        /// </summary>
        /// <returns></returns>
        public ActionResult FlowApproval()
        {
            return View();
        }

        /// <summary>
        /// 新建流程列表
        /// </summary>
        /// <returns></returns>
        public ActionResult FlowApprovalist()
        {
            return View();
        }


        /// <summary>
        /// 新建流程列表
        /// </summary>
        /// <returns></returns>
        public ActionResult FlowApprovaFrom()
        {
            return View();
        }
    }
}