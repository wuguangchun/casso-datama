﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RazorView.Controllers
{
    public class OtherController : Controller
    {
        /// <summary>
        /// 手机端图标展示
        /// </summary>
        /// <returns></returns>
        public ActionResult MobileIconShow()
        {
            return View();
        }

        /// <summary>
        /// 渐变色搭配
        /// </summary>
        /// <returns></returns>
        public ActionResult Gradients()
        {
            return View();
        }


        /// <summary>
        /// 渐变色搭配
        /// </summary>
        /// <returns></returns>
        public ActionResult Load()
        {
            return View();
        }
    }
}