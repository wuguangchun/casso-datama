




$(document).ready(function () {
    layui.use(['layer'], function () {
        var layer = layui.layer;
    });

    // bind 'myForm' and provide a simple callback function
    $("#btnSubmit").on("click", function () {

        layer.msg('请求中...', { icon: 16, shade: 0.8, time: 0 });
        $.post("/UserHandle/UserLogin", $("form").serializeArray(), function (data) {
            ShowResult(data);
        }, "json");
    });
});


function ShowResult(context) {
    //识别返回信息
    if (context.State === "Success") {
        parent.layer.msg(context.Message, { icon: 6 });
        location.reload();
    } else if (context.State === "Warning") {
        parent.layer.msg(context.Message, { icon: 7 });
    } else {
        parent.layer.alert(context.Message, { icon: 5 });
    }
}


