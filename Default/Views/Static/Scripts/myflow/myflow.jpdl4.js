(function ($) {
    var myflow = $.myflow;

    $.extend(true, myflow.config.rect, {
        attr: {
            r: 8,
            fill: '#F6F7FF',
            stroke: '#03689A',
            "stroke-width": 2
        }
    });

    $.extend(true, myflow.config.props.props, {
        name: { name: 'name', label: '名称', value: '新建流程', editor: function () { return new myflow.editors.inputEditor(); } },
        key: { name: 'key', label: '标识', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
        desc: { name: 'desc', label: '描述', value: '', editor: function () { return new myflow.editors.inputEditor(); } }
    });


    $.extend(true, myflow.config.tools.states, {
        start: {
            showType: 'image',
            type: 'start',
            name: { text: '<<start>>' },
            text: { text: '开始' },
            img: { src: '/Themes/Static/Images/myflow//48/start_event_empty.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '开始' },
                text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
            }
        },
        end: {
            showType: 'image', type: 'end',
            name: { text: '<<end>>' },
            text: { text: '结束' },
            img: { src: '/Themes/Static/Images/myflow//48/end_event_terminate.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '结束' },
                text: { name: 'text', label: '文本', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
            }
        },
        'end-cancel': {
            showType: 'image', type: 'end-cancel',
            name: { text: '<<end-cancel>>' },
            text: { text: '取消' },
            img: { src: '/Themes/Static/Images/myflow//48/end_event_cancel.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '取消' },
                text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
            }
        },
        'end-error': {
            showType: 'image', type: 'end-error',
            name: { text: '<<end-error>>' },
            text: { text: '错误' },
            img: { src: '/Themes/Static/Images/myflow//48/end_event_error.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '错误' },
                text: { name: 'text', label: '文本', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
            }
        },
        state: {
            showType: 'text', type: 'state',
            name: { text: '<<state>>' },
            text: { text: '状态' },
            img: { src: '/Themes/Static/Images/myflow//48/task_empty.png', width: 48, height: 48 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '状态' },
                text: { name: 'text', label: '描述', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
                field: { name: 'field', label: '数据源', value: '', editor: function () { return new myflow.editors.pSelectEditor("/FlowDataApi/GetAutoViewCol/?viewID=" + document.getElementById("tableData").value, "/SystemApi/GetTempByColID", "colID", "pvalue"); } },
                value: { name: 'value', label: '数据值(选)', value: '', editor: function () { return new myflow.editors.cSelectEditor(); } },
                valueText: { name: 'valueText', label: '数据值（填）', value: '', editor: function () { return new myflow.editors.inputEditor(); } }
            }
        },
        fork: {
            showType: 'image', type: 'fork',
            name: { text: '<<fork>>' },
            text: { text: '分支' },
            img: { src: '/Themes/Static/Images/myflow//48/gateway_parallel.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '分支' },
                temp1: { name: 'text', label: '描述', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
            }
        },
        join: {
            showType: 'image', type: 'join',
            name: { text: '<<join>>' },
            text: { text: '合并' },
            img: { src: '/Themes/Static/Images/myflow//48/gateway_parallel.png', width: 48, height: 48 },
            attr: { width: 50, heigth: 50 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '合并' },
                text: { name: 'text', label: '描述', value: '', editor: function () { return new myflow.editors.inputEditor(); } },
            }
        },
        task: {
            showType: 'text', type: 'task',
            name: { text: '<<task>>' },
            text: { text: '任务' },
            img: { src: '/Themes/Static/Images/myflow//48/task_empty.png', width: 48, height: 48 },
            props: {
                //text: { name: 'text', label: '显示', value: '', editor: function () { return new myflow.editors.textEditor(); }, value: '任务' }, 
                mode: { name: 'mode', label: '审批模式', value: '', editor: function () { return new myflow.editors.selectEditor([{ "Name": "指定人员", "Value": "only" }, { "Name": "指定部门(任意人)", "Value": "class" }, { "Name": "指定角色(任意人)", "Value": "role" }, { "Name": "上级或签", "Value": "role" }, { "Name": "上级会签", "Value": "role" }, { "Name": "连续上级(或签)", "Value": "role" }]); } },
                role: { name: 'role', label: '角色', value: '', editor: function () { return new myflow.editors.selectEditor('/SystemApi/GetDBPara/?tempID=99b771c2-e8db-44c8-a599-530f3e1d970F1'); } },
                user: { name: 'user', label: '人员', value: '', editor: function () { return new myflow.editors.selectEditor('/SystemApi/GetDBPara/?tempID=99b771c2-e8db-44c8-a599-530f3e1d970F'); } },
            }
        }
    });

})(jQuery);