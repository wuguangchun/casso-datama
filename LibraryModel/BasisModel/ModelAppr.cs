﻿using Newtonsoft.Json;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class ModelAppr
    {
        /**
         * 当使用Oracle 数据库时注意列类型的转换，需要转换的类型对照如下
         * "nvarchar"   "nvarchar2"
         * "datetime"   "timestamp"
         * "int"    "INTEGER"
         * "INTEGER"  "INTEGER"
         * "long"   "long"
         **/


        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "审批标志")]
        public string Approval { get; set; }


        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据归属")]
        public string DataOrg { get; set; }


        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
