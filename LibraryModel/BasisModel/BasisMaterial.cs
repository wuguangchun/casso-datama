﻿using LibraryModel.DataModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.BasisModel
{
    /// <summary>
    /// 物料管理
    /// </summary>
    public class BasisMaterial : Model
    {

        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "物料编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 500, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "名称")]
        public string Name { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "编码")]
        public string Code { get; set; }

        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "规格")]
        public string Specs { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "材质")]
        public string Material { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分类")]
        public string MaterialType { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "单位")]
        public string Company { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "安全库存")]
        public decimal SafetyStock { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "可用库存数")]
        public decimal AvailableStock { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "已用库存数")]
        public decimal UsedStock { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "占用库存数")]
        public decimal OccupyStock { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "成本价")]
        public decimal CostPrice { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "采购价")]
        public decimal PurchasePrice { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "销售价")]
        public decimal SalesPrice { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "Decimal", DecimalDigits = 2, ColumnDescription = "有效期")]
        public decimal Validity { get; set; }

        /// <summary>
        /// 1.全部检验 2.抽样检验  3.免检   4.出库时全检    4.出库时抽检
        /// </summary>
        [SugarColumn(ColumnDataType = "nvarchar", IsNullable = true, Length = 10, ColumnDescription = "质检模式")]
        public string NeedQc { get; set; }

        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "备注")]
        public string Remarks { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int", ColumnDescription = "状态")]
        public int? State { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime? CreateTime { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }

    }
}
