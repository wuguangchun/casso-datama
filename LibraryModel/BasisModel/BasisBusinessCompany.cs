﻿using SqlSugar;
using System;

namespace LibraryModel.BasisModel
{
    /// <summary>
    /// 业务往来单位
    /// </summary>
    public class BasisBusinessCompany
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "单位编号")]
        public string CompanyId { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "名称")]
        public string Name { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "电话")]
        public string Telephone { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "邮箱")]
        public string Email { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "联系人")]
        public string Contacts { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "业务类型")]
        public string BusinessType { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "等级")]
        public string Grade { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "地址")]
        public string Address { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "text", ColumnDescription = "备注")]
        public string Remarks { get; set; }

        [SugarColumn(ColumnDataType = "int", ColumnDescription = "状态")]
        public int State { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateTime { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }
    }
}
