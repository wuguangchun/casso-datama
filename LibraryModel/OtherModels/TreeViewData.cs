﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class TreeViewData
    {

        public string id { get; set; }
        public string title { get; set; }
        //public string field { get; set; }
        public object children { get; set; }
        public object Data { get; set; }
        //public string href { get; set; }
        public bool spread = true;
        //public string checked { get; set; }
        //public string disabled { get; set; }

    }
}
