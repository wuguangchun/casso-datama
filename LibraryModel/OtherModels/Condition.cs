﻿namespace LibraryModel.OtherModels
{
    /// <summary>
    /// 
    /// </summary>
    public class Condition
    {
        /// <summary>
        /// 
        /// </summary>
        public string ConditionFieldVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ConditionOptionVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ValueVal ConditionValueVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ValueVal ConditionValueLeftVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ValueVal ConditionValueRightVal { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ValueVal
    {
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Text { get; set; }
    }

    public class ConditionStr
    {
        /// <summary>
        /// 
        /// </summary>
        public string jsonStr { get; set; }
    }

    public class ConditionOption
    {
        public static string equal => "equal";
        public static string like => "like";
        public static string unlike => "unlike";
        public static string start => "start";
        public static string end => "end";
        public static string unequal => "unequal";
        public static string empty => "empty";
        public static string between => "between";
    }

}
