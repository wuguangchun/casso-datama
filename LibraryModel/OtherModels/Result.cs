﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class Result
    {
        public Result()
        {
            State = ResultState.Success;
            Message = "请求成功";
            MaCode = Guid.NewGuid().ToString();
            TimeStamp = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds);
        }
        public Result(string msg)
        {
            State = ResultState.Success;
            Message = msg;
            MaCode = Guid.NewGuid().ToString();
            TimeStamp = Convert.ToInt64((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds);
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 返回对象
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public long TimeStamp { get; set; }

        /// <summary>
        /// 返回标识
        /// </summary>
        public string MaCode { get; set; }

        public override string ToString() => JsonConvert.SerializeObject(this);

    }
}
