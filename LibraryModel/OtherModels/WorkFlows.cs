﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class WorkFlows
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Id { get; set; }
        public string Amount { get; set; }
        public string PrepareHours { get; set; }
        public string WorkingHours { get; set; }
        public string ActivePrepareHours { get; set; }
        public string ActiveWorkingHours { get; set; }
        public bool IsCurrent { get; set; }
    }
}
