﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class UploadResult
    {
        public string isSuccess { get; set; }
        public string id { get; set; }
    }
    public class AttachList
    {
        public int type { get; set; }
        public string list { get; set; } 
    }
}
