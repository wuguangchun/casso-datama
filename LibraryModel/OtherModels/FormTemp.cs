﻿using LibraryModel.DataModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{

    public class FormTemp
    {
        private string colums = "layui-col-md12";
        public FormTemp(string clo = "layui-col-md12")
        {
            colums = string.IsNullOrEmpty(clo) ? "layui-col-md12" : clo;
        }

        /// <summary>
        /// 表单控件-输入框-Input
        /// </summary> 
        public string Input(string title, string name, string id = null, string value = null, string placeholder = null, string verify = null, bool readType = false, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string placeholderHtml = !string.IsNullOrEmpty(placeholder) ? $"placeholder='{placeholder}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string readTypeHtml = readType ? $"readonly" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <input type='text' name='{name}' {idHtml} {placeholderHtml} {verifyHtml} {readTypeHtml} autocomplete='off' class='layui-input' value={value}>
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-密码框-PassWord
        /// </summary> 
        public string PassWord(string title, string name, string id = null, string value = null, string placeholder = null, string verify = null, bool readType = false, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string placeholderHtml = !string.IsNullOrEmpty(placeholder) ? $"placeholder='{placeholder}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string readTypeHtml = readType ? $"readonly" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <input type='password' name='{name}' {idHtml} {placeholderHtml} {verifyHtml} {readTypeHtml} autocomplete='off' class='layui-input' value={value}>
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-数字框-Number
        /// </summary> 
        public string Number(string title, string name, string id = null, string value = null, string placeholder = null, string verify = null, bool readType = false, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string placeholderHtml = !string.IsNullOrEmpty(placeholder) ? $"placeholder='{placeholder}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string readTypeHtml = readType ? $"readonly" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";
            value = value ?? "0";

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <input type='number' name='{name}' {idHtml} {placeholderHtml} {verifyHtml} {readTypeHtml} autocomplete='off' class='layui-input' value={value}>
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-选择框-Select
        /// </summary> 
        public string Select(string title, string name, string temp = null, List<KeyValue> options = null, string id = null, bool search = false, string verify = null, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string tempHtml = !string.IsNullOrEmpty(temp) ? $"data-temp={temp}" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            string searchHtml = search ? "lay-search" : "";
            string optionHtml = string.Empty;
            inline = string.IsNullOrEmpty(word) ? inline : "inline";

            if (options != null)
            {
                foreach (var item in options)
                {
                    optionHtml += $"<option value='{item.Value}'>{item.Name}</option>";
                }
            }

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <select  name='{name}' {idHtml} {tempHtml} {verifyHtml} {searchHtml}><option value=''>请选择</option> {optionHtml}</select>
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-选择框-Select
        /// </summary> 
        public string SelectMulti(string title, string name, string temp = null, List<KeyValue> options = null, string id = null, bool search = false, string verify = null, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string tempHtml = !string.IsNullOrEmpty(temp) ? $"data-temp={temp}" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            string searchHtml = search ? "lay-search" : "";
            string optionHtml = string.Empty;
            inline = string.IsNullOrEmpty(word) ? inline : "inline";

            if (options != null)
            {
                foreach (var item in options)
                {
                    optionHtml += $"<option value='{item.Value}'>{item.Name}</option>";
                }
            }

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <select lay-filter='{name}' xm-select='{name}'  xm-select-height='39px'  name='{name}' {idHtml} {tempHtml} {verifyHtml} {searchHtml}><option value=''>请选择</option> {optionHtml}</select>
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-时间框-DateTime
        /// </summary> 
        public string DateTime(string title, string name, string id, string value = null, string placeholder = null, string verify = null, bool readType = false, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string placeholderHtml = !string.IsNullOrEmpty(placeholder) ? $"placeholder='{placeholder}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string readTypeHtml = readType ? $"readonly" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";
            value = value ?? System.DateTime.Now.ToString();

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <input type='Datetimelocal' name='{name}' {idHtml} {placeholderHtml} {verifyHtml} {readTypeHtml} value={value} autocomplete='off' class='layui-input'>
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-单选输入框-Radio 
        /// </summary> 
        public string Radio(string title, string name, List<KeyValue> values, string id, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";
            string inpusHtml = string.Empty;

            foreach (var item in values)
            {
                inpusHtml += $@"<input name='{name}' {idHtml} type='radio'   value='{item.Value}' title='{item.Name}' />";
            }

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'> 
                                {inpusHtml}
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-单选输入框-Radio 
        /// </summary> 
        public string Checkbox(string title, string name, List<KeyValue> values, string id, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";
            string inpusHtml = string.Empty;

            foreach (var item in values)
            {
                inpusHtml += $@"<input name='{name}' {idHtml} type='checkbox'   value='{item.Value}' title='{item.Name}' />";
            }

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'> 
                                {inpusHtml}
                            </div>
                            {wordHtml}
                        </div>";
        }

        /// <summary>
        /// 表单控件-多行文本输入框-textarea 
        /// </summary> 
        public string Textarea(string title, string name, string id, string value = null, string placeholder = null, string verify = null, bool readType = false, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string placeholderHtml = !string.IsNullOrEmpty(placeholder) ? $"placeholder='{placeholder}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string readTypeHtml = readType ? $"readonly" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <textarea type='text' name='{name}' {idHtml} {placeholderHtml} {verifyHtml} {readTypeHtml} autocomplete='off' class='layui-textarea'>{value}</textarea>
                            </div>
                            {wordHtml}
                        </div>";
        }


        /// <summary>
        /// 表单控件-文件上传-textarea 
        /// </summary> 
        public string File(string title, string name, string id, string placeholder = null, string verify = null, bool readType = false, string inline = "block", string word = null)
        {
            string idHtml = !string.IsNullOrEmpty(id) ? $"id='{id}'" : "";
            string placeholderHtml = !string.IsNullOrEmpty(placeholder) ? $"placeholder='{placeholder}'" : "";
            string verifyHtml = !string.IsNullOrEmpty(verify) ? $"lay-verify='{verify}'" : "";
            string readTypeHtml = readType ? $"readonly" : "";
            string wordHtml = !string.IsNullOrEmpty(word) ? $"<div class='layui-form-mid layui-word-aux'>{word}</div>" : "";
            inline = string.IsNullOrEmpty(word) ? inline : "inline";

            return $@" <div class='layui-form-item {colums}'>
                            <label class='layui-form-label'>{title}</label>
                            <div class='layui-input-{inline}'>
                                <input type='text' style='height:32px;'  name='{name}' {idHtml} lay-filter='title' {verifyHtml}  autocomplete='off' placeholder='{placeholderHtml}' {readTypeHtml} {readTypeHtml} class='layui-input uploadFile'>                  
                                <div class='layui-progress' lay-filter='{name}'>
                                    <div class='layui-progress-bar' lay-percent='0%'></div>
                                </div>
                            </div>
                            {wordHtml}
                        </div>";
        }
    }

}
