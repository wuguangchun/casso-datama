﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class FlowModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, Rect> states { get; set; }

        public Dictionary<string, Paths> paths { get; set; }
    }

    public class Paths
    {
        public string from { get; set; }

        public string to { get; set; }
    }

    public class Rect
    {
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Text text { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Attr attr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Props props { get; set; }
    }
    public class Props
    {
        /// <summary>
        /// 
        /// </summary>
        public Text text { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Field field { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Value value { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ValueText valueText { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Mode mode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Role role { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Role branch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public User user { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public User cc { get; set; }
    }


    public class Text
    {
        /// <summary>
        /// 开始
        /// </summary>
        public string text { get; set; }
    }

    public class Attr
    {
        /// <summary>
        /// 
        /// </summary>
        public int x { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int y { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int width { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int height { get; set; }
    }

    public class Field
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    public class Value
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    public class ValueText
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    public class Mode
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    public class Role
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    public class User
    {
        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    public class Name
    {
        /// <summary>
        /// 新建流程
        /// </summary>
        public string value { get; set; }
    }

    public class Key
    {
        /// <summary>
        /// 标识字段测试
        /// </summary>
        public string value { get; set; }
    }

    public class Desc
    {
        /// <summary>
        /// 描述字段测试
        /// </summary>
        public string value { get; set; }
    }

    public class from
    {
        /// <summary>
        /// 开始
        /// </summary>
        public string text { get; set; }
    }
    public class to
    {
        /// <summary>
        /// 开始
        /// </summary>
        public string text { get; set; }
    }

}
