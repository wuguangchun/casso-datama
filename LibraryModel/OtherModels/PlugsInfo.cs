﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class PlugsInfo
    {
        public string PlugName { get; set; }
        public string PlugVersion { get; set; }
        public string PlugDesc { get; set; }
    }
}
