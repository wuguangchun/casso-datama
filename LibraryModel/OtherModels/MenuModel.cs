﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class MenuModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string icon { get; set; }
        public string href { get; set; }
        public bool spread { get; set; }
        public List<MenuModel> children { get; set; }
        public string target { get; set; }
        public string name { get; set; }

    }
}
