﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class KeyValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string ParentID { get; set; }
    }
}
