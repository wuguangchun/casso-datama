﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{
    public class Approval
    {
        /// <summary>
        /// 会审查看权限
        /// </summary>
        public bool JointTrial { get; set; }

        /// <summary>
        /// 审批权限
        /// </summary>
        public bool Approvals { get; set; }

        /// <summary>
        /// 明细查看权限
        /// </summary>
        public bool Detailed { get; set; }

        /// <summary>
        /// 邀请会审权限
        /// </summary>
        public bool JoinInvitation { get; set; }
    }
}
