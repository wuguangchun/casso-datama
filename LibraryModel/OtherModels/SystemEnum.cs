﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.OtherModels
{

    public class ResultState
    {
        public static string Success => "Success";
        public static string Error => "Error";
        public static string Warning => "Warning";
    }

    public class MessageType
    {
        public static string RabbitMQ => "RabbitMQ";
        public static string Mqtt => "Mqtt";
        public static string WorkWeiXin => "WorkWeiXin";
        public static string AppMessage => "AppMessage";
        public static string Whole => "Whole";
    }

}
