﻿using LibraryModel.DataModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.Appr
{
   public  class ApprForm : Model
    {
        [SugarColumn(IsPrimaryKey = true, IsNullable = false, Length = 50,  ColumnDataType = "nvarchar", ColumnDescription = "编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段1")]
        public string ColumnStr1 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段2")]
        public string ColumnStr2 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段3")]
        public string ColumnStr3 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段4")]
        public string ColumnStr4 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段5")]
        public string ColumnStr5 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段6")]
        public string ColumnStr6 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段7")]
        public string ColumnStr7 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段8")]
        public string ColumnStr8 { get; set; }

        [SugarColumn(Length = 100,  ColumnDataType = "nvarchar", ColumnDescription = "字段9")]
        public string ColumnStr9 { get; set; }

        [SugarColumn(  ColumnDataType = "text", ColumnDescription = "文本1")]
        public string ColumnText1 { get; set; }

        [SugarColumn( ColumnDataType = "text", ColumnDescription = "文本2")]
        public string ColumnText2 { get; set; }

        [SugarColumn( ColumnDataType = "text", ColumnDescription = "文本3")]
        public string ColumnText3 { get; set; }

        [SugarColumn( ColumnDataType = "text", ColumnDescription = "文本4")]
        public string ColumnText4 { get; set; }

        [SugarColumn(  ColumnDataType = "text", ColumnDescription = "文本5")]
        public string ColumnText5 { get; set; }

        [SugarColumn( ColumnDataType = "int", ColumnDescription = "数字1")]
        public int ColumnInt1 { get; set; }

        [SugarColumn( ColumnDataType = "int", ColumnDescription = "数字2")]
        public int ColumnInt2 { get; set; }

        [SugarColumn(  ColumnDataType = "int", ColumnDescription = "数字3")]
        public int ColumnInt3 { get; set; }

        [SugarColumn(  ColumnDataType = "int", ColumnDescription = "数字4")]
        public int ColumnInt4 { get; set; }

        [SugarColumn( ColumnDataType = "int", ColumnDescription = "数字5")]
        public int ColumnInt5 { get; set; }

        [SugarColumn(  ColumnDataType = "DateTime", ColumnDescription = "日期1")]
        public DateTime ColumnDate1 { get; set; }

        [SugarColumn(ColumnDataType = "DateTime", ColumnDescription = "日期2")]
        public DateTime ColumnDate2 { get; set; }

        [SugarColumn(ColumnDataType = "DateTime", ColumnDescription = "日期3")]
        public DateTime ColumnDate3 { get; set; }

        [SugarColumn(ColumnDataType = "DateTime", ColumnDescription = "日期4")]
        public DateTime ColumnDate4 { get; set; }

        [SugarColumn(ColumnDataType = "DateTime", ColumnDescription = "日期5")]
        public DateTime ColumnDate5 { get; set; }

        [SugarColumn(Length = 50, ColumnDataType = "nvarchar", ColumnDescription = "视图编号")]
        public string AutoViewID { get; set; }

        [SugarColumn(Length = 50, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }

        [SugarColumn(ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

    }
}
