﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysToolBarFunction : Model
    {

        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ToolBarID { get; set; }

        [SugarColumn(Length = 2000, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Function { get; set; }

        [SugarColumn(ColumnDataType = "datetime")]
        public DateTime CreateDate { get; set; }
        
        [SugarColumn(Length = 50, ColumnDataType = "nvarchar")]
        public string CreateUser { get; set; }

        
    }
}
