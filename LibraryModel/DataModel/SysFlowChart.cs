﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysFlowChart : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "流程编号")]
        public string FlowId { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "流程名称")]
        public string Name { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "标识")]
        public string Key { get; set; }

        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "介绍")]
        public string desc { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "原文")]
        public string Json { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "历史版本")]
        public string JsonL1 { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "历史版本2")]
        public string JsonL2 { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "历史版本3")]
        public string JsonL3 { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "历史版本4")]
        public string JsonL4 { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "历史版本5")]
        public string JsonL5 { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "历史版本6")]
        public string JsonL6 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "授权可见")]
        public string ReadUser { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }


        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime updateDate { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

    }
}
