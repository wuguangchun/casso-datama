﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysLogDataExec
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "日志ID")]
        public string LogID { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "内容")]
        public string Content { get; set; } 

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "耗时")]
        public string SqlExecTime { get; set; } 
        
        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "执行时间")]
        public DateTime CreateDate { get; set; }

    }
}
