﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysFlowApprovalDetail : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "审批明细编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "审批编号")]
        public string ApprovalID { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int", ColumnDescription = "顺序号")]
        public int sort { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "模式")]
        public string Mode { get; set; }

        [SugarColumn(Length = 3000, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "执行人")]
        public string Executor { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "审批人")]
        public string Reviewer { get; set; }

        [SugarColumn(Length = 3000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "抄送人")]
        public string CCperson { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "备注")]
        public string Remarks { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "状态")]
        public string State { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "审批时间")]
        public string ApprovalDate { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建时间")]
        public string CreateDate { get; set; }
    }
}
