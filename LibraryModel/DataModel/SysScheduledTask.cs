﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysScheduledTask
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "任务ID")]
        public string TaskID { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "任务地址")]
        public string TaskUri { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "任务名称")]
        public string TaskName { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "任务状态")]
        public int TaskState { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "最后执行时间")]
        public DateTime ExecTime { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "执行效率")]
        public string Efficiency { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "执行规则")]
        public int Role { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "执行结果")]
        public string Result { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "DateTime" , ColumnDescription = "创建日期")]
        public DateTime CreateDate { get; set; }

    }
}
