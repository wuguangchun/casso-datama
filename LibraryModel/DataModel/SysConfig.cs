﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysConfig : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "配置项ID")]
        public string CfgId { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "配置项编码")]
        public string CfgCode { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "配置项名称")]
        public string CfgName { get; set; }

        [SugarColumn(Length = 5000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "值")]
        public string CfgValue { get; set; }

        [SugarColumn(Length = 5000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "显示值")]
        public string CfgDisValue { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "介绍")]
        public string CfgDesc { get; set; }
        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "状态")]
        public int CfgState { get; set; }
    }
}

