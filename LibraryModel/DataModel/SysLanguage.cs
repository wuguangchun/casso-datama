﻿using SqlSugar;
using System;

namespace LibraryModel.DataModel
{
    public class SysLanguage : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar",ColumnDescription ="语言ID")]
        public string LanguageID { get; set; }

        [SugarColumn( Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "类型")]
        public string LanguageType { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "编码")]
        public string LanguageCode { get; set; }

        [SugarColumn(Length = 1000, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "值")]
        public string LanguageValue { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建用户")]
        public string CreateUser { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }
    }
}
