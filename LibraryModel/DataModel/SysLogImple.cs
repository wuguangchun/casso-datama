﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysLogImple
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "系统执行日志ID")]
        public string LogID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "模块类")]
        public string ModuleClass { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "模块方法")]
        public string Module { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "内容")]
        public string Content { get; set; } 

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "操作用户")]
        public string ExecUser { get; set; } 
        
        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "操作日期")]
        public DateTime CreateDate { get; set; }

    }
}
