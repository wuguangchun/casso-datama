﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysAutoView : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "自动视图ID")]
        public string ActionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "视图名称")]
        public string AutoViewName { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据源")]
        public string DBTable { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "树数据源")]
        public string DBTableTree { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据主键")]
        public string DBTableKey { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "树ID")]
        public string TreeViewID { get; set; }

        [SugarColumn(Length = 200, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "数据源地址")]
        public string TableUrl { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "树数据源地址")]
        public string TreeUrl { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据修改地址")]
        public string EditUrl { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据删除地址")]
        public string DeleteUrl { get; set; }

        [SugarColumn(Length = 10, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "分页")]
        public string Page { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "默认工具")]
        public string DefaultToolbar { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "高度")]
        public string Height { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "宽度")]
        public string Width { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "加载图标")]
        public string Loading { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "合计行")]
        public string TotalRow { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分页数据")]
        public string Limit { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分页数据")]
        public string Limits { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "默认排序")]
        public string InitSort { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "空数据转换")]
        public string TextEmpty { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "多用户数据")]
        public string Multi { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "审批支持")]
        public string Approval { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int", ColumnDescription = "数据库序号")]
        public int DataBase { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "多表格支持")]
        public string MultipleTables { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建用户")]
        public string CreateUser { get; set; }

        [SugarColumn(ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

    }
}
