﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class UViewColunms: Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string UViewColunmId { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ViewColunmId { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string CreateUser { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ActionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Field { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Title { get; set; }

        [SugarColumn(IsNullable = true, Length = 50, ColumnDataType = "nvarchar")]
        public string Width { get; set; }

        [SugarColumn(IsNullable = true, Length = 50, ColumnDataType = "nvarchar")]
        public string MinWidth { get; set; }

        [SugarColumn(IsNullable = true,  ColumnDataType = "int")]
        public int Sorting { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ColType { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Fixed { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Hide { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Sort { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Unresize { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string TotalRow { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Edit { get; set; }
        [SugarColumn(IsNullable = true, ColumnDataType = "int")]
        public int ReadOnly { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Event { get; set; }
        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Style { get; set; }
        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar")]
        public string IsQuery { get; set; }
        [SugarColumn(Length = 30, IsNullable = true, ColumnDataType = "nvarchar")]
        public string EditType { get; set; }
        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Required { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Align { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Templet { get; set; }
        [SugarColumn(Length = 2000, IsNullable = true, ColumnDataType = "nvarchar")]
        public string TempFormat { get; set; }
        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar")]
        public string DefaultVal { get; set; }
    }
}
