﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysFlow : Model
    {
        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "流程编号")]
        public string ID { get; set; }

        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "视图编号")]
        public string ActionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "审批字段")]
        public string ApprovalField { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "流程名称")]
        public string Name { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "标识")]
        public string Key { get; set; }

        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "介绍")]
        public string desc { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "原文")]
        public string Json { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

    }
}
