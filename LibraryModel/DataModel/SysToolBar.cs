﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysToolBar : Model
    {
        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ActionID { get; set; }

        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ToolBarID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ToolBarName { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string ToolType { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int")]
        public int Sort { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Event { get; set; }

        [SugarColumn(Length = 150, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Class { get; set; }

    }
}
