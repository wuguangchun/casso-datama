﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysDevice : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "终端ID")]
        public string DeviceId { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "终端名称")]
        public string DeviceName { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "终端类型")]
        public string DeviceType { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "公司")]
        public string Company { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "部门")]
        public string Department { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "组织")]
        public string Team { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "职位")]
        public string Position { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "状态")]
        public int State { get; set; }
    }
}

