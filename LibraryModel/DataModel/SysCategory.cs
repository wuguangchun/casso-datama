﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysCategory : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "分类ID")]
        public string Id { get; set; }

        [SugarColumn(Length = 20, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分类编码")]
        public string CategoryCode { get; set; }

        [SugarColumn(Length = 30, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分类名称")]
        public string CategoryName { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "父级ID")]
        public string ParentID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "组织")]
        public string OrgAttrId { get; set; }


    }
}
