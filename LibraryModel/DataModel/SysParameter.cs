﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysParameter : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "参数ID")]
        public string TempID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "名称")]
        public string TempName { get; set; }

        [SugarColumn(Length = 2000, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "数据源")]
        public string TempContext { get; set; }

        [SugarColumn(Length = 10, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "类型")]
        public string TempType { get; set; }

        [SugarColumn( IsNullable = false, ColumnDataType = "int", ColumnDescription = "数据库序号")]
        public int DataBase { get; set; }

        [SugarColumn(ColumnDataType = "datetime", ColumnDescription = "创建日期")]
        public DateTime CreateDate { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }
    }
}
