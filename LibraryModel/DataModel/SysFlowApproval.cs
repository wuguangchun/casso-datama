﻿using SqlSugar;
using System;

namespace LibraryModel.DataModel
{
    public class SysFlowApproval : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "审批编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "流程标题")]
        public string Title { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "数据标识")]
        public string DataId { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "数据集")]
        public string DataSet { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "视图编号")]
        public string ActionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "发起人")]
        public string Sponsor { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "操作")]
        public string Operation { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "状态")]
        public string State { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }
    }
}
