﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace LibraryModel.DataModel
{
    public class SysAutoChart
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string CharID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string CharViewName { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text")]
        public string DataSourse { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar")]
        public string IsSql { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text")]
        public string Option { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int")]
        public int Interval { get; set; }


        [SugarColumn(IsNullable = false, ColumnDataType = "int")]
        public int DataBase { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string CreateUser { get; set; }

        [SugarColumn(ColumnDataType = "datetime")]
        public DateTime CreateDate { get; set; }
    }
}
