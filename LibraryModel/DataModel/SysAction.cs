﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
   public class SysAction : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "系统视图ID")]
        public string Id { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "视图名称")]
        public string ActionName { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "视图地址")]
        public string ActionUrl { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "锁定状态")]
        public string Locked { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "权限")]
        public string Jurisdiction { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "父级ID")]
        public string ParentID { get; set; }

        [SugarColumn(ColumnDataType = "int", ColumnDescription = "排序")]
        public int? Short { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "图标")]
        public string Icon { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "重要视图")]
        public string Crux { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "移动端")]
        public string Mobile { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "是否显示在菜单栏")]
        public string ShowMenu { get; set; } 
        
    }
}
