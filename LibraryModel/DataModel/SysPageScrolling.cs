﻿using LibraryModel.DataModel;
using SqlSugar;
using System;

namespace LibraryModel.DataModel
{
    public class SysPageScrolling : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "轮播页面编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "页面名称")]
        public string Name { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "页面编号")]
        public string PID { get; set; }

        [SugarColumn(Length = 500, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "页面地址")]
        public string URL { get; set; }

        [SugarColumn(ColumnDataType = "int", ColumnDescription = "状态")]
        public int State { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateTime { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }
    }
}
