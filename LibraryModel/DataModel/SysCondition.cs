﻿using LibraryModel.DataModel;
using SqlSugar;

namespace LibraryModel.OtherModels
{
    /// <summary>
    /// 
    /// </summary>
    public class SysCondition : Model
    {
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ConditionFieldVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string ConditionOptionVal { get; set; }
    }


}
