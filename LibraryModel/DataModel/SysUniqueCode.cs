﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysUniqueCode : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "唯一码ID")]
        public string UniqueID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "码类型")]
        public string CodeType { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "唯一码")]
        public string UniqueCode { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateTime { get; set; }
    }
}