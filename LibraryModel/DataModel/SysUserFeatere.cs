﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysUserFeatere
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]

        public string ID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string UID { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar")]
        public IntPtr Featere { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text")]
        public string FacePhoto { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime")]
        public DateTime CreateDate { get; set; }

    }
}
