﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysUserRole : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Id { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string GroupName { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string OrgAttrId { get; set; }

        [SugarColumn(ColumnDataType = "int")]
        public int Lock { get; set; }

        [SugarColumn(Length = 8000, IsNullable = true, ColumnDataType = "nvarchar")]
        public string HavingPage { get; set; }
    }
}
