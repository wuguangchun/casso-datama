﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysFileManager : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "文件ID")]
        public string ID { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "文件地址")]
        public string FilePath { get; set; }

        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "原名称")]
        public string FileName { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "大小")]
        public int FileSize { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "类型")]
        public string FileType { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "状态")]
        public int State { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }
    }
}

