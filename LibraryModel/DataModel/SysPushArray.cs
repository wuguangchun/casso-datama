﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysPushArray : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "消息编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "消息状态")]
        public string State { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "消息类型")]
        public string Type { get; set; }

        [SugarColumn(Length = 500, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "订阅者")]
        public string Receiver { get; set; }

        [SugarColumn(Length = 100, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "消息信息")]
        public string Message { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "text", ColumnDescription = "消息主体")]
        public string Object { get; set; }

        [SugarColumn(Length = 100, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "时间戳")]
        public string TimeStamp { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "DateTime", ColumnDescription = "推送时间")]
        public DateTime PushDate { get; set; }
    }
}
