﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysAutoReport : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "报表视图ID")]
        public string ReportActionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "报表名称")]
        public string ReportViewName { get; set; }

        [SugarColumn( IsNullable = true, ColumnDataType = "text", ColumnDescription = "数据源（SQL）")]
        public string DataSourse { get; set; }
        
        [SugarColumn(Length = 100, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "是否分页")]
        public string Page { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "默认工具")]
        public string DefaultToolbar { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据表高度")]
        public string Height { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "宽度")]
        public string Width { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "是否显示加载图标")]
        public string Loading { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "是否显示合计行")]
        public string TotalRow { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分页数据")]
        public string Limit { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分页数据")]
        public string Limits { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "默认排序")]
        public string InitSort { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "空转换")]
        public string TextEmpty { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int", ColumnDescription = "数据库序号")]
        public int DataBase { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }

        [SugarColumn(ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

    }
}
