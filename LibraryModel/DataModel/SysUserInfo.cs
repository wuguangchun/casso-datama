﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysUserInfo : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Uid { get; set; }
        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Photo { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string UserName { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string PassWord { get; set; }
        [SugarColumn(Length = 100, ColumnDataType = "nvarchar")]
        public string Nick { get; set; }
        [SugarColumn(Length = 15, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Telphone { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string Email { get; set; }
        [SugarColumn(ColumnDataType = "int", IsNullable = true)]
        public int Sex { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar")]
        public string RoleId { get; set; }

        [SugarColumn(Length = 500, IsNullable = false, ColumnDataType = "nvarchar")]
        public string OrgAttrId { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Superior { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Language { get; set; }
        [SugarColumn(ColumnDataType = "datetime")]
        public DateTime CreateTime { get; set; }
        [SugarColumn(ColumnDataType = "datetime", IsNullable = true)]
        public DateTime LastTime { get; set; }
        [SugarColumn(ColumnDataType = "int")]
        public int Status { get; set; }

        //作为扩展
        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar")]
        public string WeUserId { get; set; }

        [SugarColumn(Length = 500, IsNullable = true, ColumnDataType = "nvarchar")]
        public string AccessToken { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar")]
        public string AppClientId { get; set; }
    }
}
