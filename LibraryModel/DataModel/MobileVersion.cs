﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class MobileVersion : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "编号")]
        public string  ID { get; set; }

        [SugarColumn( ColumnDataType = "int", DefaultValue = "0", ColumnDescription = "升级标识")]
        public int update_flag { get; set; }
          
        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "强制升级")]
        public int forceupdate { get; set; }

        [SugarColumn(IsNullable = true, Length = 200, ColumnDataType = "nvarchar", ColumnDescription = "包文件")]
        public string update_url { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "升级说明")]
        public string update_tips { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = " 版本")]
        public string version { get; set; }
         
        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "体积大小")]
        public int size { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "增量升级")] 
        public int wgt_flag { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "增量包")]
        public string wgt_url { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "平台")]
        public string platform { get; set; }
        

        [SugarColumn(IsNullable = true, ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }
    }
}
