﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class UknowledgeBase : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar")]
        public string ID { get; set; }

        [SugarColumn(Length = 200, IsNullable = false, ColumnDataType = "nvarchar")]
        public string Title { get; set; }

        [SugarColumn( IsNullable = false, ColumnDataType = "text")]
        public string Context { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar")]
        public string File { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int")]
        public int KnowType { get; set; }

        [SugarColumn(Length = 200, IsNullable = false, ColumnDataType = "nvarchar")]
        public string CreateUser { get; set; }

        [SugarColumn(Length = 200, IsNullable = false, ColumnDataType = "nvarchar")]
        public string CreateDate { get; set; }


    }
}
