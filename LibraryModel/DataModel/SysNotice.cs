﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysNotice : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar",ColumnDescription ="公告ID")]
        public string NoticeId { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar",ColumnDescription = "标题")]
        public string Title { get; set; }

        [SugarColumn(Length = 600, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "介绍")]
        public string Description { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "text", ColumnDescription = "内容")]
        public string ContentBody { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "公告级别")]
        public int Noticelevel { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "公告类型")]
        public int NoticeType { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "接收组织")]
        public string ReceiveOrg { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "接收角色")]
        public string ReceiveRole { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "接收人")]
        public string ReceiveUser { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "int", ColumnDescription = "状态")]
        public int State { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建用户")]
        public string CreateUser { get; set; }

        [SugarColumn( IsNullable = true, ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateTime { get; set; }
    }
}
