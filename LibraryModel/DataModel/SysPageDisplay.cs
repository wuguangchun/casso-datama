﻿using LibraryModel.DataModel;
using SqlSugar;
using System;


namespace LibraryModel.DataModel
{
    public class SysPageDisplay : Model
    {
        [SugarColumn(Length = 50, IsPrimaryKey = true, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "页面编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "页面名称")]
        public string Name { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "滚动方式")]
        public string ScrollingMode { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "切换时间")]
        public string SwitchingTime { get; set; }

        [SugarColumn(ColumnDataType = "int", IsNullable = false, ColumnDescription = "状态")]
        public int State { get; set; }

        [SugarColumn(IsNullable = true, ColumnDataType = "DateTime", ColumnDescription = "创建时间")]
        public DateTime CreateTime { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建人")]
        public string CreateUser { get; set; }
    }
}
