﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysAutoTreeView : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "树视图ID")]
        public string TreeViewId { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "视图名称")]
        public string TreeName { get; set; }
        [SugarColumn( Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "数据绑定树ID项")]
        public string Id { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据主键")]
        public string PrimaryKey { get; set; }
        [SugarColumn( Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "数据显示名称项")]
        public string Title { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "顶级数据示范")]
        public string TopLevelVal { get; set; }
        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "父级项")]
        public string Parent { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "点击链接")]
        public string Href { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "是否可用")]
        public string Disabled { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "多选框")]
        public string ShowCheckbox { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "修改图标")]
        public string Edit { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "展开")]
        public string Accordion { get; set; }
        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "连接线")]
        public string ShowLine { get; set; }

    }
}
