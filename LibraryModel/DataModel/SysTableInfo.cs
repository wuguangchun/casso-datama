﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysTableInfo : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "编号")]
        public string ID { get; set; }

        [SugarColumn(Length = 100, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "表名")]
        public string TableName { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主键列")]
        public string PrimaryKey { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "创建用户列")]
        public string CreateUserCol { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "关联人")]
        public string PersonCol { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "角色列")]
        public string RoleCol { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "修改用户列")]
        public string UpdateUserCol { get; set; }

        [SugarColumn(Length = 1000, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "组织列")]
        public string OrgCol { get; set; }
    }
}
