﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel.DataModel
{
    public class SysAutoMTables : Model
    {
        [SugarColumn(IsPrimaryKey = true, Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "多表格视图编号")]
        public string MactionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "自动视图ID")]
        public string ActionID { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "视图名称")]
        public string AutoViewName { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据源")]
        public string DBTable { get; set; }


        [SugarColumn(Length = 200, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "数据源地址")]
        public string TableUrl { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主键")]
        public string DBTableKey { get; set; }

        [SugarColumn(Length = 200, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "数据删除地址")]
        public string DeleteUrl { get; set; }

        [SugarColumn(Length = 10, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "分页")]
        public string Page { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "高度")]
        public string Height { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "宽度")]
        public string Width { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "加载图标")]
        public string Loading { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "合计行")]
        public string TotalRow { get; set; }

        [SugarColumn(Length = 10, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分页数据")]
        public string Limit { get; set; }

        [SugarColumn(Length = 100, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "分页数据")]
        public string Limits { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "默认排序")]
        public string InitSort { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "空数据转换")]
        public string TextEmpty { get; set; }

        [SugarColumn(IsNullable = false, ColumnDataType = "int", ColumnDescription = "数据库序号")]
        public int DataBase { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主表列")]
        public string MainField { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "辅表列")]
        public string ConditionField { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主表列1")]
        public string MainField1 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "辅表列1")]
        public string ConditionField1 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主表列2")]
        public string MainField2 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "辅表列2")]
        public string ConditionField2 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主表列3")]
        public string MainField3 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "辅表列3")]
        public string ConditionField3 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "主表列4")]
        public string MainField4 { get; set; }

        [SugarColumn(Length = 50, IsNullable = true, ColumnDataType = "nvarchar", ColumnDescription = "辅表列4")]
        public string ConditionField4 { get; set; }

        [SugarColumn(Length = 50, IsNullable = false, ColumnDataType = "nvarchar", ColumnDescription = "创建用户")]
        public string CreateUser { get; set; }

        [SugarColumn(ColumnDataType = "datetime", ColumnDescription = "创建时间")]
        public DateTime CreateDate { get; set; }

    }
}
